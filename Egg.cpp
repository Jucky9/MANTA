#include "Egg.hpp"


Egg::Egg(int turnHatching,int stimuliType): Agent(0,0,stimuliType) , _turnHatching(turnHatching+(rand() % (turnHatching/3)+1)) ,_hatching(false) {
	_weight = 1;
}

Egg::Egg(int turnHatching,Square* myPosition,int stimuliType): Agent(0,0,myPosition,stimuliType) , _turnHatching(turnHatching+(rand() % (turnHatching/3)+1)) ,_hatching(false) {
	_weight = 1;
	normalStimuli();
}

void Egg::act(){
	if(_isAlive){
		incTurn();
		if (!_hatching and _turn==_turnHatching){
			if(_myPosition)
				hatching();
			else
				_turn--;
		}
	}
}

bool Egg::isHatching()const{
	return _hatching;
}

void Egg::hatching(){
	_hatching=true;
	death();	
}

void Egg::display(){
	if(_isAlive)
		std::cout<<" E ";
	else{
		std::cout<<"MOR";
	}
}
