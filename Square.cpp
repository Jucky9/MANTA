#include "Square.hpp"
#include "Trace.hpp"


Square::Square(Object* Object,int posI,int posJ,bool typeSquare,float stimuliResi) :_trace(nullptr),_stimuli(),_objectOnSquare(),_neighbourSquare(),_posI(posI),_posJ(posJ),_typeSquare(typeSquare),_isCheck(false),_totalWeight(0),_stimuliResi(stimuliResi) {
	_objectOnSquare.push_back(Object);
	_totalWeight = Object->getWeight();
	for(int i=0; i<8;i++){
		_neighbourSquare.push_back(nullptr);
	}
}


Square::Square(int posI,int posJ,bool typeSquare,float stimuliResi) : _trace(nullptr),_stimuli(),_objectOnSquare(),_neighbourSquare(),_posI(posI),_posJ(posJ),_typeSquare(typeSquare),_isCheck(false),_totalWeight(0),_stimuliResi(stimuliResi){
	for(int i=0; i<8;i++){
		_neighbourSquare.push_back(nullptr);
	}
}

bool Square::canGoIn(int weight){
	return _typeSquare and objectCanBeAdd(weight) ;
}

bool Square::addObjectOnSquare(Object* object){
	if (objectCanBeAdd(object)){
		_objectOnSquare.push_back(object);
		_totalWeight = _totalWeight + object->getWeight();
		return true;
	}
	return false;
}

bool Square::objectCanBeAdd(int object){
	return (_totalWeight + object<= MAXWEIGHT );
}

bool Square::objectCanBeAdd(Object* object){
	return (_totalWeight + object->getWeight() <= MAXWEIGHT );
}
void Square::removeWeight(int object){
	_totalWeight-=object;

}
void Square::removeObjectOnSquare(Object* object){
	std::vector<Object*>::iterator it(_objectOnSquare.begin());
	while(it!=_objectOnSquare.end() and (*it)!=object){
		it++;
	}
	if (it!=_objectOnSquare.end() and (*it) == object){
		_objectOnSquare.erase(it);
		_totalWeight =std::max(_totalWeight - object->getWeight(),0);
	}
	else{
		std::cout<<"Objet pas trouvé"<<std::endl;
		throw 1;
	}

}

void Square::addNeighbours(Square *neighbours,int posNeighb){
	std::vector<Square*>::iterator it(_neighbourSquare.begin());
	it+=posNeighb;
	_neighbourSquare.erase(it);
	_neighbourSquare.insert(it,neighbours);
	it=_neighbourSquare.begin();
	while(it!=_neighbourSquare.end()){
		it++;
	}
}


Square* Square::getRightNeighbor(){
	return _neighbourSquare[2];
}

Square* Square::getUpNeighbor(){ // Voisin du dessus
	return _neighbourSquare[0];
}
Square* Square::getBackNeighbor()//Voisin du dessous
{
	return _neighbourSquare[4];
}
Square* Square::getLeftNeighbor() // Voisin de gauche
{
	return _neighbourSquare[6];
}
Square* Square::getUpRightNeighbor() // Voisin de diagonal : en haut à droite
{
	return _neighbourSquare[1];
}
Square* Square::getUpLeftNeighbor() // Voisin de diagonal : en haut à gaucge
{
	return _neighbourSquare[7];
}
Square* Square::getBackLeftNeighbor() // Voisin de diagonal : en bas à gauche
{
	return _neighbourSquare[5];
}
Square* Square::getBackRightNeighbor() // Voisin de diagonal : en bas à droite
{
	return _neighbourSquare[3];
}
Square* Square::getNeighbor(int neighbour){
	return _neighbourSquare[neighbour];
}
void Square::display(){
	if(_objectOnSquare.size()){
		_objectOnSquare.at(_objectOnSquare.size()-1)->display();
	}
	else{
		std::cout<<"   ";
	}

}
void Square::addTrace(Trace* newTr){
	if(_trace)
		delete _trace;
	_trace=newTr;
}

void Square::removeTrace( Trace* old){
	if (old==_trace){
		delete _trace;
		_trace=nullptr;
	}
}

Trace* Square::getTrace(){
	return _trace;
}

void Square::aging(){
	if(_trace)
		_trace->aging();
}

bool Square::getIsCheck(){
	return _isCheck;
}
void Square::setIsCheck(bool check){
	_isCheck=check;
}

std::shared_ptr<Stimuli> Square::getStimuli(int i){
	return _stimuli.at(i);
}

int Square::getStimuliSize(){
	return static_cast<int>(_stimuli.size());
}
void Square::addStimuli(std::shared_ptr<Stimuli> newSti){
	std::vector<std::shared_ptr<Stimuli>>::iterator it(_stimuli.begin());
	while(it!=_stimuli.end() and (*it)->getType()<newSti->getType()){
			it++;
		}
	if(it!=_stimuli.end() and (*it)->getType()==newSti->getType()){
		if(*newSti>*(*it)){
			_stimuli.erase(it);
			it=_stimuli.insert(it,newSti);
		}
	}
	else{
		it=_stimuli.insert(it,newSti);
	}
}

void Square::removeStimuli(Stimuli* stimuliRemove){
	auto it=_stimuli.begin();
	while(it!=_stimuli.end() and (*it)->getType()<stimuliRemove->getType()){
		it++;
	}
	if(it!=_stimuli.end()and (*it)->getType()==stimuliRemove->getType()){
		_stimuli.erase(it);
	}
	else{
		std::cout<<"Pas Trouvé"<<std::endl;
	}
}

void Square::linkObject(){
	std::vector<Object*>::iterator it(_objectOnSquare.begin());
	while(it!=_objectOnSquare.end()){
		(*it)->setPos(this);
		it++;

	}
}
void Square::endStimuli(Stimuli* old){
	auto it=_stimuli.begin();
	while(it!=_stimuli.end() and (*it)->getType()!=old->getType()){
		it++;
	}
	if(it!=_stimuli.end()and (*it)->notSameRoot(old))
		(*it)->propagationStimuli((*it));
	auto itObj=_objectOnSquare.begin();
	while (itObj!=_objectOnSquare.end()){
		(*itObj)->squareFreeStimuli(old->getType());
		itObj++;
	}
}

int Square::getPosI(){
	return _posI;
}
int Square::getPosJ(){
	return _posJ;
}

bool Square::haveStymuliType(int type){
	auto it=_stimuli.begin();
	while(it!=_stimuli.end() and (*it)->getType()<type){
		it++;
	}
	return it!=_stimuli.end() and (*it)->getType()==type;
}
float Square::getSimuliResi(){
	return _stimuliResi;
}

bool Square::getType(){
	return _typeSquare;
}
float Square::getStimuliPower(int type){
	float power(0);
	auto it=_stimuli.begin();
	while(it!=_stimuli.end() and (*it)->getType()<type){
		it++;
	}
	if(it!=_stimuli.end() and (*it)->getType()==type){
		power=(*it)->getPower();
	}
	return power;
}

Square::~Square(){
	if(_trace){
		delete _trace;
		_trace = nullptr;
	}
	while(_objectOnSquare.begin()!=_objectOnSquare.end()){
		delete (*_objectOnSquare.begin());
		_objectOnSquare.erase(_objectOnSquare.begin());
	}
}