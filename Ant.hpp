#ifndef MACROOBJECTIVE
#define MACROOBJECTIVE
#define NORMAL 0
#define SEARCHFOOD 1
#define GOBACK 2
#define FEEDINGLARVA 3
#define DEADBODY 4
#define DEADROOM 5
#define FOODROOM 6
#define EGG 7
#define EGGROOM 8
#define QUENEN 9
#define HAVEFOOD 10
#define PUPA 11
#define PUPAROOM 12
#define MAKEAROOM 15
#define FOLLOWTRACE 18
#define GOBACKSEARCH 19


#define VALLEARNING 0.1

#endif

#ifndef ANT_HPP
#define ANT_HPP

#include "ActiveAgent.hpp"
#include "Leef.hpp"
#include "Trace.hpp"

#include "Egg.hpp"
#include "Pupa.hpp"
#include "Larva.hpp"
#include <queue>


class Ant: public ActiveAgent{
	public:
		Ant(int,int,int,int,int,Square*,int,int,int);
		Ant(const Ant&)=delete;
		Ant& operator=(const Ant&)=delete;
		virtual void act() override;
		virtual void display() override;
		virtual void info();
		~Ant()=default;
	protected:
		int _target;
		Object* _carry;
		std::vector<float> _learning;
		std::weak_ptr<Stimuli> _currentStimuli;
		std::vector<std::shared_ptr<Stimuli>> _allStimuli;
		bool _needFood;
		int _traceType;
		std::vector<Trace*> _allTrace;
		bool _makeFirstTastk;
		bool _firstGoBack;
		bool _ignoreStimuli;
		int _turnIgnore;
		int _foodTraceLifetime;
		int _ignoringTrace;
		int _normalTraceLaps;
		virtual void chooseObjective();
		virtual void followObjective();
		virtual void followTrace();
		Square* findLowerTrace(int,bool);
		Square* findStongerTrace(int,bool);
		bool haveFindTraceType(int,bool);

		virtual std::shared_ptr<Stimuli> findStrongerStimuli();
		virtual void takeStimuli(std::weak_ptr<Stimuli>);

		virtual void goBack();

		virtual void careLarva();
		virtual void carePupa();
		virtual void careEgg();


		virtual void feedingLarva();

		virtual bool moveSquare(Square*)override;
		virtual void search();

		virtual void takeObject(Object*);
		virtual void putObject(Square *);

		virtual void death()override;
		virtual bool distance(Square*,Square*,unsigned int);
		virtual bool packObject();
		virtual void incTurn();
		virtual bool isOutSide();
		virtual void feed(AgentEater*);
		virtual void goToObjective()override;
		virtual void normalAction();
		virtual void deletFood(Object*);
		virtual void creatPath(Square*);
	private:
		class Node{
			public:
				Node(Square*,int);
				Square* getSquare();
				int getLevel();
				~Node()=default;
			private:
				Square* _pos;
				int _level;
			
		};



};		


#endif /* ANT_HPP */