#ifndef Ground_h
#define Ground_h

#include "Object.hpp"
#include "Obstacle.hpp"

class Ground : public Obstacle{
public:
	Ground(bool);
	void display()override;
	~Ground()=default;
};
#endif