#ifndef Wall_h
#define Wall_h

#include "Object.hpp"
#include "Obstacle.hpp"

class Wall : public Obstacle{
public:
	Wall(bool);
	void display()override;
	~Wall()=default;
};
#endif