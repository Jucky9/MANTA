#include "ClickableLabel.hpp"

ClickableLabel::ClickableLabel() : QLabel(){}

 
ClickableLabel::~ClickableLabel()
{
}
 
void ClickableLabel::mousePressEvent(QMouseEvent*)
{	
    emit clicked();
}