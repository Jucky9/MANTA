#include "ObjectGUI.hpp"


ObjectGUI::ObjectGUI(QString name):QObject(),_label(nullptr),_namePix(name),_pic(name),_mtx(){}

QLabel* ObjectGUI::getLabel(){
	return _label;
}

ObjectGUI::~ObjectGUI(){
	delete _label;
	_label=nullptr;
}

void ObjectGUI::deleteLabel(){
	QPixmap temp(QSize(20,20));
	temp.fill(Qt::transparent);
	_label->setPixmap(temp);
}

void ObjectGUI::normalPic(){
	_label->setPixmap(_pic);
	_label->raise();
}

QString ObjectGUI::getPixname(){
	return _namePix;
}