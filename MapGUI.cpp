#include "MapGUI.hpp"


MapGUI::MapGUI(std::string MapNumb,QGridLayout* window, std::string leaf, std::string seed, QMainWindow* mainWind,float outS,float inS,int seedNutrition,int leafNutrition) :Map(MapNumb,outS,inS,false),_window(window), 
          leafImage(leaf), seedImage(seed), mainWindow(mainWind),_seedNutrition(seedNutrition),_leafNutrition(leafNutrition){
	std::string line;
  std::vector<int> test;
  std::ifstream myfile (MapNumb);
  if (myfile.is_open())
  {
    int count = 0;
    while ( getline (myfile,line) ){
      if(count!=0){
        createObjectsByLine(line,count-1);
      }
      count++;
    }
  }
   QObject::connect(this,SIGNAL(squareClickedSignal(SquareGUI*)),this,SLOT(showOption(SquareGUI*)));
}
MapGUI::~MapGUI(){
	delete fenetre;
}

void MapGUI::showOption(SquareGUI* selected){
  selectedSquare = selected;
  i = selectedSquare->getPosI(); j = selectedSquare->getPosJ(); 
  fenetre = new QWidget();
  fenetre->setGeometry(1600,680,300,500);
  QTabWidget *onglets = new QTabWidget(fenetre);
  onglets->setStyleSheet("QTabWidget::tab-bar{left:0px; }\n"
"QTabBar::tab{height:40px; width:105px; color:blue;font: 9pt}\n");
  QWidget *page1 = new QWidget (onglets);
  QGridLayout *grille1 = new QGridLayout(page1);
  seedSchoice(grille1,0,0);
  onglets->addTab(page1,"seed");
  QWidget *page2 = new QWidget (fenetre);
  QGridLayout *grille2 = new QGridLayout(page2);
  leafSchoice(grille2,0,0);
  grille2->setAlignment(Qt::AlignTop);
  onglets->addTab(page2, "leaf");
  validate = new QPushButton(" validate ");
  validate->setStyleSheet("background-color: #4CAF50; color: white; font-size:20px; border: #4CAF50; padding: 4px; ");
  validate->setCursor(Qt::PointingHandCursor);
  grille1->addWidget(validate, 4, 1);
    validate2 = new QPushButton(" validate ");
  validate2->setStyleSheet("background-color: #4CAF50; color: white; font-size:20px; border: #4CAF50; padding: 4px; ");
  validate2->setCursor(Qt::PointingHandCursor);
  grille2->addWidget(validate2, 4, 1);
  QObject::connect(validate, SIGNAL(clicked()), this, SLOT(validateChoices()));
  QObject::connect(validate2, SIGNAL(clicked()), this, SLOT(validateChoices()));
  fenetre->show();
}
void MapGUI::seedSchoice (QGridLayout * grille1, int x, int y){
  std::string res;
  QGroupBox *group = new QGroupBox ("Seed's category");
  seedSchoice1 = new QRadioButton("Pivot's seed");
  seedSchoice2 = new QRadioButton("Niger's seed");
  seedSchoice3 = new QRadioButton("Quinoa's seed");
  seedSchoice4 = new QRadioButton("Amaranthe's seed");
  QVBoxLayout *vbox3x = new QVBoxLayout;
  vbox3x->addWidget (seedSchoice1);
  vbox3x->addWidget (seedSchoice2);
  vbox3x->addWidget (seedSchoice3);
  vbox3x->addWidget (seedSchoice4);
  group->setLayout(vbox3x);
  grille1->addWidget (group, x, y);
  QGroupBox *group2 = new QGroupBox ("Seed's Images");
  QLabel *free = new QLabel();
  free->setScaledContents(true);
  free->setStyleSheet("background:transparent ;");
  free->setPixmap(QPixmap("seed.png"));
  free->setFixedWidth(80);
  free->setFixedHeight(70);
  QLabel *free1 = new QLabel();
  free1->setScaledContents(true);
  free1->setStyleSheet("background:transparent ;");
  free1->setPixmap(QPixmap("seed2.png"));
  free1->setFixedWidth(80);
  free1->setFixedHeight(70);
  QLabel *free2 = new QLabel();
  free2->setScaledContents(true);
  free2->setStyleSheet("background:transparent ;");
  free2->setPixmap(QPixmap("seed3.png"));
  free2->setFixedWidth(80);
  free2->setFixedHeight(70);
  QLabel *free3 = new QLabel();
  free3->setScaledContents(true);
  free3->setStyleSheet("background:transparent ;");
  free3->setPixmap(QPixmap("seed4.png"));
  free3->setFixedWidth(80);
  free3->setFixedHeight(70);
  QVBoxLayout *vbox3x2 = new QVBoxLayout;
  vbox3x2->addWidget (free);
  vbox3x2->addWidget (free1);
  vbox3x2->addWidget (free2);
   vbox3x2->addWidget (free3);
  group2->setLayout(vbox3x2);
  grille1->addWidget (group2, 0, 1);
  
}

void MapGUI::validateChoices(){
  //std::vector<Square*>::iterator itCurrent(_mapSquare.at(i).begin());
  if (seedSchoice1->isChecked()){
    emit createSeedSignal(5,false,_seedNutrition,"seed.png",selectedSquare);
  }
  else if (seedSchoice2->isChecked()){
    emit createSeedSignal(5,false,_seedNutrition,"seed2.png",selectedSquare);
  }
  else if (seedSchoice3->isChecked()){
    emit createSeedSignal(5,false,_seedNutrition,"seed3.png",selectedSquare);
  }
  else if (seedSchoice4->isChecked()){
    emit createSeedSignal(5,false,_seedNutrition,"seed4.png",selectedSquare);
  }
  else if (leafSchoice1->isChecked()){
    emit createLeafSignal(5,false,_leafNutrition,"leaf.png",selectedSquare);
  }
  else if (leafSchoice2->isChecked()){
    emit createLeafSignal(5,false,_leafNutrition,"leaf2.png",selectedSquare);
  }
  else if (leafSchoice3->isChecked()){
    emit createLeafSignal(5,false,_leafNutrition,"leaf3.png",selectedSquare);
  }
  else if (leafSchoice4->isChecked()){
    emit createLeafSignal(5,false,_leafNutrition,"leaf4.png",selectedSquare);
  }
  selectedSquare=nullptr;
  fenetre->close();
  delete fenetre;
  fenetre=nullptr;
}

void MapGUI::leafSchoice (QGridLayout * grille1, int x, int y){

    QGroupBox *group = new QGroupBox ("Leaf's category");
    leafSchoice1 = new QRadioButton("Hortensia");
    leafSchoice2 = new QRadioButton("Hortie");
    leafSchoice3 = new QRadioButton("Salad");
    leafSchoice4 = new QRadioButton("Cresson");
    QVBoxLayout *vbox3x = new QVBoxLayout;
    vbox3x->addWidget (leafSchoice1);
    vbox3x->addWidget (leafSchoice2);
    vbox3x->addWidget (leafSchoice3);
    vbox3x->addWidget (leafSchoice4);
    group->setLayout(vbox3x);
    grille1->addWidget (group, x, y);
    QGroupBox *group2 = new QGroupBox ("Seed's Images");
    QLabel *free = new QLabel();
  free->setScaledContents(true);
  free->setStyleSheet("background:transparent ;");
  free->setPixmap(QPixmap("leaf.png"));
  free->setFixedWidth(80);
  free->setFixedHeight(70);
  QLabel *free1 = new QLabel();
  free1->setScaledContents(true);
  free1->setStyleSheet("background:transparent ;");
  free1->setPixmap(QPixmap("leaf2.png"));
  free1->setFixedWidth(80);
  free1->setFixedHeight(70);
  QLabel *free2 = new QLabel();
  free2->setScaledContents(true);
  free2->setStyleSheet("background:transparent ;");
  free2->setPixmap(QPixmap("leaf3.png"));
  free2->setFixedWidth(80);
  free2->setFixedHeight(70);
  QLabel *free3 = new QLabel();
  free3->setScaledContents(true);
  free3->setStyleSheet("background:transparent ;");
  free3->setPixmap(QPixmap("leaf4.png"));
  free3->setFixedWidth(80);
  free3->setFixedHeight(70);
    QVBoxLayout *vbox3x2 = new QVBoxLayout;
    vbox3x2->addWidget (free);
    vbox3x2->addWidget (free1);
    vbox3x2->addWidget (free2);
     vbox3x2->addWidget (free3);
    group2->setLayout(vbox3x2);
    grille1->addWidget (group2, 0, 1);

}

void MapGUI::createObjectsByLine(std::string line, int ligne){
  _mapSquare.push_back(std::vector<Square*>());
  line.erase(std::remove(line.begin(), line.end(), ' '),line.end());//clears the string and erases the spaces
  std::vector<Square*>::iterator itCurrent(_mapSquare.at(ligne).begin());
  std::vector<Square*>::iterator itPrevious;
  if (ligne>0){
    itPrevious= _mapSquare.at(ligne-1).begin();
  }
  SquareGUI* temp = nullptr;
  for (unsigned int i =0; i<line.size(); i++){
  	switch(line[i]){
      case WALL:{
      	temp=new SquareGUI(new Wall(false),ligne,i,false,line[i],(_outSide ? STIMULOUTSTRONG : STIMULINSTRONG ),_window);
      	_window->addWidget(temp->getLabel(),ligne,i);
        itCurrent=_mapSquare.at(ligne).insert(itCurrent,temp);
        break;
      }
      case SEED:{
      	SeedGUI *seedTemp = new SeedGUI(5,false,_seedNutrition,seedImage);
      	temp=new SquareGUI(seedTemp,ligne,i,true,line[i],(_outSide ? STIMULOUTSTRONG : STIMULINSTRONG ),_window);
      	_window->addWidget(temp->getLabel(),ligne,i);
      	_window->addWidget(seedTemp->getLabel(),ligne,i);
        itCurrent=_mapSquare.at(ligne).insert(itCurrent,temp);

        break;
      }
      case GROUND:{
        _outSide=false;
      	temp=new SquareGUI(new Wall(false),ligne,i,false,line[i],(_outSide ? STIMULOUTSTRONG : STIMULINSTRONG ),_window);
      	_window->addWidget(temp->getLabel(),ligne,i);
        itCurrent=_mapSquare.at(ligne).insert(itCurrent,temp);
        break;
      }
      case LEEF:{
      	LeafGUI *leafTemp = new LeafGUI(5,true,_leafNutrition,leafImage);
      	temp=new SquareGUI(leafTemp,ligne,i,true,line[i],(_outSide ? STIMULOUTSTRONG : STIMULINSTRONG ),_window);
      	_window->addWidget(temp->getLabel(),ligne,i);
      	_window->addWidget(leafTemp->getLabel(),ligne,i);
        itCurrent=_mapSquare.at(ligne).insert(itCurrent,temp);
        break;
      }
      default :{
      	temp=new SquareGUI(ligne,i,true,line[i],(_outSide ? STIMULOUTSTRONG : STIMULINSTRONG ),_window);
      	_window->addWidget(temp->getLabel(),ligne,i);
        itCurrent=_mapSquare.at(ligne).insert(itCurrent,temp);
        if (line[i]==SPAWN){
          _spawn.push_back(*itCurrent);
        }
        break;
      }
    }
    QObject::connect(temp,SIGNAL(addGUIObject(Object*)),temp,SLOT(addGUIObjectSlot(Object*)));
    QObject::connect(temp,SIGNAL(removeGUIObject(Object*)),temp,SLOT(removeGUIObjectSlot(Object*)));
    QObject::connect(temp,SIGNAL(clicked(SquareGUI*)),this,SLOT(squareClicked(SquareGUI*)));
    if(ligne){
      if(i){
        (*itCurrent)->addNeighbours(*(itPrevious-1),7);/*diagonal gauche haut*/
        (*(itPrevious-1))->addNeighbours(*(itCurrent),3);/*diagonal droit bas*/
      }

      (*itCurrent)->addNeighbours(*(itPrevious),0);/*haut*/
      (*itPrevious)->addNeighbours(*(itCurrent),4);/*bas*/

      if(i+1!=line.size()){
        (*itCurrent)->addNeighbours(*(itPrevious+1),1);/*diagonal haut droit*/
        (*(itPrevious+1))->addNeighbours(*(itCurrent),5);/*diagonal bas gauch*/
      }
    }
    if(i){
      (*itCurrent)->addNeighbours(*(itCurrent-1),6);/*gauche*/
      (*(itCurrent-1))->addNeighbours(*itCurrent,2);/*droite*/
    }
    itCurrent++;
    if (ligne>0){
      itPrevious++;
    }
  }
}

QGridLayout* MapGUI::getGrid(){
  return _window;
}
void  MapGUI::squareClicked(SquareGUI* clicked){
  if(fenetre){
    selectedSquare=nullptr;
    fenetre->close();
    delete fenetre;
    fenetre=nullptr;
  }
  emit squareClickedSignal(clicked);

}