#include "Ant.hpp"


Ant::Ant(int range,int turnHungry, int nbrDeathEating, int frequenMove,int turnDeath ,Square* Pos ,int foodTraceLifetime,int ignoringTrace,int normalTraceLaps): ActiveAgent(0,range,turnHungry,nbrDeathEating,frequenMove,turnDeath,Pos,-1), _target(NORMAL),_carry(nullptr),
			_learning(),_currentStimuli(),_allStimuli(),_needFood(false),_traceType(0),_allTrace(),_makeFirstTastk(false),_firstGoBack(true),_ignoreStimuli(false),_turnIgnore(0),_foodTraceLifetime(foodTraceLifetime),
			_ignoringTrace(ignoringTrace),_normalTraceLaps(normalTraceLaps){
	for(int i=0;i<21;i++){
		_learning.push_back(0);
	}
		_weight = 4;
}

Ant::Node::Node(Square* pos,int level):_pos(pos),_level(level){}

Square* Ant::Node::getSquare(){
	return _pos;
}
int Ant::Node::getLevel(){
	return _level;
}

void Ant::act(){
	if(isAlive()){
		incTurn();
		if(!(_turnEating%getTurnHungry())){
			hungry();
		}
		if(_turn==_turnDeath){
			death();
		}
		if(dynamic_cast<Food*>(_carry) and isHungry()){
			feed(this);
		}
		else if(isAlive()){
			if (!(_turn%_frequenMove)){
				normalAction();
			}
		}
	}
	if(_carry){
		if(4+_carry->getWeight()!=getWeight()){
			_weight=4+_carry->getWeight();
		}
	}
	else{
		if(getWeight()!=4){
			_weight=4;
		}
	}
}

void Ant::normalAction(){
	chooseObjective();
	_allStimuli.clear();
	followObjective();
	_allTrace.clear();
}

void Ant::chooseObjective(){
	std::shared_ptr<Stimuli> newSti;
	newSti=findStrongerStimuli();
	auto currentStimuli=_currentStimuli.lock();
	if(!currentStimuli or (currentStimuli and ((currentStimuli->getPower()+_learning.at(currentStimuli->getType()))<(newSti->getPower()+_learning.at(newSti->getType())) or ( currentStimuli->getOrinin() and !currentStimuli->getOrinin()->canGoIn(_weight))))){
		takeStimuli(newSti);
	}
}

void Ant::followObjective(){
	_traceType=0;
	if(_carry and dynamic_cast<Food*>(_carry)){
		_traceType=1;
	}
	switch(_target){
		case NORMAL:
			_path.clear();
			randomMove();
			if(_carry and inARoom(15))
				_target=MAKEAROOM;
			break;
		case SEARCHFOOD:
			if (_path.size())
				goToObjective();
			else {
				auto currentSti=_currentStimuli.lock();
				if(currentSti){
					if(_myPosition==currentSti->getRoot()->getPos()){
						takeObject(currentSti->getRoot());
						_target=GOBACK;
						_firstGoBack=true;
						_stimuliType=HAVEFOOD;
						_stimuliPower=5;
						normalStimuli();
					}
					else{
						currentSti=std::make_shared<Stimuli>(new Stimuli());
						_target=NORMAL;
					}
					
				}
			}
			break;
		case FOODROOM:
			if (_path.size()){
				goToObjective();
			}
			else {
				auto currentSti=_currentStimuli.lock();
				if(currentSti){
					putObject(_myPosition);
					_firstGoBack=true;
					_learning.at(FOODROOM)+=VALLEARNING;
					_learning.at(SEARCHFOOD)+=VALLEARNING;
				}
			}
			break;
		case GOBACK:
			goBack();
			if(_carry and inARoom(15)){
				_target=MAKEAROOM;
				_path.clear();
			}
			break;
		case GOBACKSEARCH:
			randomMove();
			if(haveFindTraceType(1,false) or haveFindTraceType(0,false)){
				_target=GOBACK;
			}
			break;
		case MAKEAROOM:
			if(inARoom(15)){
				if(!_path.size()){
					if(!_makeFirstTastk){
						moveAwayDoor();
					}
					else{
						goToWall();
					}
				}
				goToObjective();
				if(!_path.size()){
					if(!_makeFirstTastk){
						_makeFirstTastk=true;
					}
					else{
						putObject(_myPosition);
						_makeFirstTastk=false;
						_firstGoBack=true;
					}
				}
			}
			else{
				_target=NORMAL;
				_currentStimuli=std::make_shared<Stimuli>(new Stimuli());
				randomMove();
				_makeFirstTastk=false;	
			}
			break;
		case FEEDINGLARVA:
			if(_carry and dynamic_cast<Food*>(_carry)){
				if (_path.size()){
					goToObjective();
				}
				else {
					auto currentSti=_currentStimuli.lock();
					if(currentSti){
						feed(dynamic_cast<AgentEater*>(currentSti->getRoot()));
						if(_carry and isOutSide()){
							_target=GOBACK;
						}
						_learning.at(FEEDINGLARVA)+=VALLEARNING;
					}
				}
			}
			break; 
		case DEADBODY:
			if (_path.size()){
				goToObjective();
			}
			else {
				auto currentSti=_currentStimuli.lock();
				if(currentSti){
					if(_myPosition==currentSti->getRoot()->getPos()){
						takeObject(currentSti->getRoot());
						if(isOutSide()){
							_target=GOBACK;
						}
						else{
							_target=NORMAL;
							_currentStimuli=std::make_shared<Stimuli>(new Stimuli());
						}
					}
					else{
						currentSti=std::make_shared<Stimuli>(new Stimuli());
						_target=NORMAL;
					}
				}
			}
			break;
		case EGG:
			if (_path.size()){
				goToObjective();

			}
			else{
				auto currentSti=_currentStimuli.lock();
				if(currentSti){
					if(_myPosition==currentSti->getRoot()->getPos()){
						takeObject(currentSti->getRoot());
					}
					else{
						currentSti=std::make_shared<Stimuli>(new Stimuli());
						_target=NORMAL;
					}
				}
			}
			break;
		case EGGROOM:
			if (_path.size()){
				goToObjective();
			}
			else{
				auto currentSti=_currentStimuli.lock();
				if(currentSti and _carry){
					putObject(_myPosition);
					_learning.at(EGG)+=VALLEARNING;
					_learning.at(EGGROOM)+=VALLEARNING;
				}
			}
			break;
		case PUPA:
			if (_path.size()){
				goToObjective();
			}
			else{
				auto currentSti=_currentStimuli.lock();
				if(currentSti){
					if(_myPosition==currentSti->getRoot()->getPos()){
						takeObject(currentSti->getRoot());
					}
					else{
						currentSti=std::make_shared<Stimuli>(new Stimuli());
						_target=NORMAL;
					}
				}
			}
			break;
		case PUPAROOM:
			if (_path.size()){
				goToObjective();
			}
			else{
				auto currentSti=_currentStimuli.lock();
				if(currentSti and _carry){
					putObject(_myPosition);
					_learning.at(PUPA)+=VALLEARNING;
					_learning.at(PUPAROOM)+=VALLEARNING;
				}
			}
			break;
		case DEADROOM:
			if (_path.size()){
				goToObjective();
			}
			else{
				auto currentSti=_currentStimuli.lock();
				if(currentSti and _carry){
					putObject(_myPosition);
					_learning.at(DEADBODY)+=VALLEARNING;
					_learning.at(DEADROOM)+=VALLEARNING;
					
				}
			}
			break;
		case FOLLOWTRACE:
			followTrace();
			break;
		case HAVEFOOD:
			if (_path.size()){
				goToObjective();
			}
			else{
				_target=NORMAL;
				_currentStimuli=std::make_shared<Stimuli>(new Stimuli());
			}
	}
}

std::shared_ptr<Stimuli> Ant::findStrongerStimuli(){
	search();
	_needFood=isHungry();
	auto it(_allStimuli.begin());
	while(it!=_allStimuli.end() and (*it)->getType()<HAVEFOOD){
		it++;
	}
	bool someoneHaveFood(it!=_allStimuli.end() and (*it)->getType()==HAVEFOOD);
	auto bestStimuli(std::make_shared<Stimuli>(new Stimuli()));
	if(_target==GOBACK){
		bestStimuli=std::make_shared<Stimuli>(new Stimuli(GOBACK,0,nullptr,this));
	}
	else if(_target==MAKEAROOM){
		bestStimuli=std::make_shared<Stimuli>(new Stimuli(MAKEAROOM,3,nullptr,this));
	}
	else if(_target==GOBACKSEARCH){
		bestStimuli=std::make_shared<Stimuli>(new Stimuli(GOBACKSEARCH,0,nullptr,this));
	}
	if(_allStimuli.size()){
		it=_allStimuli.begin();
		Agent* tempA=dynamic_cast<Agent*>(_carry);
		while(it!=_allStimuli.end()){
			auto current=(*it);
			switch(current->getType()){
				case SEARCHFOOD:
					if (!_carry){
						if(current->getPower()+_learning.at(current->getType())>bestStimuli->getPower()+_learning.at(bestStimuli->getType())){
							bestStimuli=current;
						}
					}
					break;
				case FOODROOM:
					if((_carry and dynamic_cast<Food*>(_carry)) or (_needFood and !_carry and !someoneHaveFood)){
						if(current->getPower()+_learning.at(current->getType())>bestStimuli->getPower()+_learning.at(bestStimuli->getType())){
							bestStimuli=current;
						}
					}
					break;
				case FEEDINGLARVA:
					_needFood=true;
					if(_carry and dynamic_cast<Food*>(_carry)){
						if(current->getPower()+_learning.at(current->getType())>bestStimuli->getPower()+_learning.at(bestStimuli->getType())){
							bestStimuli=current;
						}
					}
					break;
				case EGGROOM:
					if(_carry and dynamic_cast<Egg*>(_carry) and !dynamic_cast<Pupa*>(_carry) and tempA and tempA->isAlive()){
						if(current->getPower()+_learning.at(current->getType())>bestStimuli->getPower()+_learning.at(bestStimuli->getType())){
							bestStimuli=current;
						}
					}
					break;
				case PUPAROOM:
					if(_carry and(dynamic_cast<Pupa*>(_carry) or dynamic_cast<Larva*>(_carry)) and tempA and tempA->isAlive()){
						if(current->getPower()+_learning.at(current->getType())>bestStimuli->getPower()+_learning.at(bestStimuli->getType())){
							bestStimuli=current;
						}
					}
					break;
				case DEADBODY:
					if(!_carry){
						if(current->getPower()+_learning.at(current->getType())>bestStimuli->getPower()+_learning.at(bestStimuli->getType())){
							bestStimuli=current;
						}
					}
					break;
				case DEADROOM:
					if(_carry and tempA and !tempA->isAlive()){
						if(current->getPower()+_learning.at(current->getType())>bestStimuli->getPower()+_learning.at(bestStimuli->getType())){
							bestStimuli=current;
						}
					}
					break;
				case EGG:
					if(!_carry){
						if(current->getPower()+_learning.at(current->getType())>bestStimuli->getPower()+_learning.at(bestStimuli->getType())){
							bestStimuli=current;
						}
					}
					break;
				case PUPA:
					if(!_carry){
						if(current->getPower()+_learning.at(current->getType())>bestStimuli->getPower()+_learning.at(bestStimuli->getType())){
							bestStimuli=current;
						}
					}
					break;
				case HAVEFOOD:
					if(isHungry() and !_carry){
						if(current->getPower()+_learning.at(current->getType())>bestStimuli->getPower()+_learning.at(bestStimuli->getType()) or bestStimuli->getType()==FOODROOM){
							bestStimuli=current;
						}
					}
					break;
			}
			it++;
		}
	}
	if(!_ignoreStimuli){
		bool t1(haveFindTraceType(1,true));
		if(bestStimuli->getType()==NORMAL and t1 and !(dynamic_cast<Agent*>(_carry) and dynamic_cast<Agent*>(_carry)->isAlive())){
			bestStimuli=std::make_shared<Stimuli>(new Stimuli(FOLLOWTRACE,2,nullptr,this));
		}
	}
	else{
		if(bestStimuli->getOrinin()){
			_ignoreStimuli=false;
		}
	}
	return bestStimuli;
}

void Ant::goBack(){
	Square* goal(nullptr);
	bool t1(haveFindTraceType(1,_firstGoBack));
	if(t1){
		goal=findStongerTrace(1,_firstGoBack);
	}
	if(!t1 or !goal){
		goal=findLowerTrace(0,_firstGoBack);
	}
	if(goal){
		creatPath(goal);
		if(_firstGoBack)
			_firstGoBack=false;
		goToObjective();
	}
	else{
		_target=GOBACKSEARCH;
	}
}

void Ant::followTrace(){
	Square* goal(nullptr);
	bool t1(haveFindTraceType(1,_firstGoBack));
	if(t1){
		goal=findStongerTrace(1,_firstGoBack);
	}
	if(goal){
		creatPath(goal);
		if(_firstGoBack)
			_firstGoBack=false;
		goToObjective();
	}
	else{
		_ignoreStimuli=true;
		_target=NORMAL;
		_currentStimuli=std::make_shared<Stimuli>(new Stimuli());
		randomMove();
	}
	
}

Square* Ant::findLowerTrace(int type,bool back){
	int mov=typeLastMove();
	Trace* lower= new Trace(nullptr,-1,type);//represente le max possible
	auto it=_allTrace.begin();
	while(it!=_allTrace.end()){
		if((*it)->getType()==type and (lower->getTime()==-1 or lower->getTime()>(*it)->getTime()) and (back or !(InBack((*it)->getPos(),mov)))){
			lower=(*it);
		}
		it++;
	}
	return lower->getPos();
}
Square* Ant::findStongerTrace(int type,bool back){
	int mov=typeLastMove();
	Trace* lower= new Trace(nullptr,0,type);//represente le minn possible
	Trace* front=new Trace(nullptr,0,type);
	bool pathInBack(false);
	auto it=_allTrace.begin();
	while(it!=_allTrace.end()){
		if((*it)->getType()==type){
			if(!back){
				pathInBack=pathInBAck((*it)->getPos(),mov);
			}
			if(InFront((*it)->getPos(),mov) and front->getTime()<(*it)->getTime()){
				if(!pathInBack){
					front=(*it);
				}
			}
			else if((back or (!pathInBack and  distance(_myPosition,(*it)->getPos(),2) and !(InBack((*it)->getPos(),mov)))) and lower->getTime()<(*it)->getTime()){
				lower=(*it);
			}
		}
		it++;
	}
	if(front->getTime() and (1.5*front->getTime())>=lower->getTime()){
		lower=front;
	}
	return lower->getPos();
}

bool Ant::haveFindTraceType(int type,bool back){
	auto it=_allTrace.begin();
	int mov=typeLastMove();
	while (it!=_allTrace.end() and !((*it)->getType()==type and (back or !InBack((*it)->getPos(),mov)))){
		it++;
	}
	return it!=_allTrace.end() and (*it)->getType()==type;
}


void Ant::careLarva(){std::cout<<"Je m'occupe d'une larve "<<std::endl;}
void Ant::carePupa(){std::cout<<"Je m'occupe d'un cocon "<<std::endl;}
void Ant::careEgg(){std::cout<<"Je m'occupe d'un oeuf"<<std::endl;}


void Ant::feedingLarva(){
	auto currentStimuli=_currentStimuli.lock();
	if(currentStimuli)
		dynamic_cast<AgentEater*>(currentStimuli->getRoot())->eat(dynamic_cast<Food*>(_carry));
}

void Ant::takeStimuli(std::weak_ptr<Stimuli> a){
	auto sti=a.lock();
	if(sti){
		_target=sti->getType();
		if(_target==FOODROOM and _needFood){
			_target=SEARCHFOOD;
		}
		_currentStimuli=a;
		if(sti->getOrinin()){// stimuli sans lieux
			creatPath(sti->getOrinin());
		}
	}
}

void Ant::display(){
	if(isAlive()){
		std::cout<<_target<<"A";
		if(_carry){
			if(dynamic_cast<Food*>(_carry))
				std::cout<<"f";
			else
				std::cout<<"a";
		}
		else
			std::cout<<" ";
	}
	else{
		std::cout<<"MOR";
	}
}

bool Ant::moveSquare(Square* next){
	bool moveOk=ActiveAgent::moveSquare(next);
	if(moveOk){	
		if(_lastSquare!=_myPosition){
			new Trace(_lastSquare,(_traceType ?_foodTraceLifetime:_normalTraceLaps),_traceType);
		}
		if(_stimuliType){
			normalStimuli();
		}
		return true;
	}
	return false;
}

void Ant::goToObjective(){
	if(_path.size()){
		if (moveSquare(*(_path.begin()))){
			_path.erase(_path.begin());
		}
		else{
			auto currentStimuli=_currentStimuli.lock();
			if(currentStimuli and currentStimuli->getOrinin()){
				creatPath(currentStimuli->getOrinin());
				if(_path.size()){
					if (moveSquare(*(_path.begin()))){
						_path.erase(_path.begin());
					}
				}
			}
		}
	}
}

void Ant::search(){
	std::queue<Node> working;
	std::queue<Node> end;
	std::vector<std::shared_ptr<Stimuli>>::iterator it;
	std::shared_ptr<Stimuli> checking(nullptr);
	Square* next(nullptr);
	Node current(_myPosition,_range);
	current.getSquare()->setIsCheck(true);
	working.push(current);
	std::shared_ptr<Stimuli> compareStimuli;
	Trace *temp;
	while(working.size()){
		current=working.front();
		working.pop();
		if(current.getLevel()-1){
			for(int i=0;i<8;i++){
				next=current.getSquare()->getNeighbor(i);
				if(next and next->canGoIn(_weight) and !(next->getIsCheck())){
					next->setIsCheck(true);
					working.push(Node(next,current.getLevel()-1));
					temp=next->getTrace();
					if(temp){
						_allTrace.push_back(temp);
					}
				}
			}
		}
		
		it=_allStimuli.begin();
		if(it!=_allStimuli.end()){
			compareStimuli=(*it);
		}
		for(int i=0;i<current.getSquare()->getStimuliSize();i++){
			checking=current.getSquare()->getStimuli(i);
			while(it!=_allStimuli.end() and compareStimuli->getType()<checking->getType()){
				it++;
				if(it!=_allStimuli.end())
					compareStimuli=(*it);
			}
			if(it!=_allStimuli.end() and compareStimuli->getType()==checking->getType() ){
				if((*checking)>(*compareStimuli)){
					_allStimuli.erase(it);
					it=_allStimuli.insert(it,checking);
				}
			}
			else{
				if(it==_allStimuli.end()){
					compareStimuli=checking;
				}
				it=_allStimuli.insert(it,checking);
			}
		}
		end.push(current);
	}
	while(end.size()){
		current=end.front();
		end.pop();
		current.getSquare()->setIsCheck(false);
	}
}


void Ant::info(){
	std::cout<<" Objective "<<_target<< " Stimuli "<<std::endl;
	auto temp=_currentStimuli.lock();
	if(temp)
		temp->display();
	else{
		std::cout<<"Pas de stimuli"<<std::endl;
	}
	std::cout<<" Trace "<<_traceType<< " MonStymuli type "<<_stimuliType<<std::endl;
}

void Ant::takeObject(Object* toBeCarried){
	if(_myPosition==toBeCarried->getPos()){
		_weight+=toBeCarried->getWeight();
		_carry=toBeCarried;
		_carry->toBeCarried();
	}
}
void Ant::putObject(Square * position){
	if(isAlive())
		_carry->incStimuliType();
	_myPosition->removeWeight(_carry->getWeight());
	_carry->putOn(position);
	_weight-=_carry->getWeight();
	_carry=nullptr;
	_target=NORMAL;
	_currentStimuli=std::make_shared<Stimuli>(new Stimuli());
	if(!isHungry()){
		_stimuliType=-1;
		normalStimuli();
	}


}
void Ant::death(){
	ActiveAgent::death();
	if(_carry)
		putObject(_myPosition);
}

bool Ant::distance(Square* pos,Square* obj ,unsigned int dist){
	if(int(dist*2)>heuristic(pos,obj)){
		Astar temp(pos,obj,0);
		return temp.getPath().size()<=dist;
	}
	return false;
}
bool Ant::packObject(){
	int previousSquare(-1);
	Square* next(nullptr);
	for(int i=1;i<8;i+=2){
		next=_myPosition->getNeighbor(i);
		if(next and next->getType() and next->canGoIn(0)){
			previousSquare=i;
		}
	}
	if(previousSquare){
		next=_myPosition->getNeighbor(previousSquare+(rand()%2)-1);
		if(next){
			_path.push_back(next);
		}
	}
	return previousSquare;
}
void Ant::incTurn(){
	ActiveAgent::incTurn();
	if(_ignoreStimuli){
		_turnIgnore++;
		if(_turnIgnore==_ignoringTrace){
			_turnIgnore=0;
			_ignoreStimuli=false;
		}
	}
}
bool Ant::isOutSide(){
	if(_myPosition)
		return !(_myPosition->haveStymuliType(DEADROOM) or _myPosition->haveStymuliType(FOODROOM) or _myPosition->haveStymuliType(EGGROOM) or _myPosition->haveStymuliType(PUPAROOM) or _myPosition->haveStymuliType(QUENEN));
	return true;

}

void Ant::feed(AgentEater* target){
	target->eat(dynamic_cast<Food*>(_carry));
	if(!(dynamic_cast<Food*>(_carry)->getPortion())){
		_weight-=_carry->getWeight();
		deletFood(_carry);
		_carry=nullptr;
		_target=NORMAL;
		_currentStimuli=std::make_shared<Stimuli>(new Stimuli());
		_stimuliType=-1;
		normalStimuli();
	}
}
void Ant::deletFood(Object* obj){
	delete obj;
}
void Ant::creatPath(Square* next){
	Astar temp(_myPosition,next,_weight);
	_path=temp.getPath();
}