#ifndef QUEEN_HPP
#define QUEEN_HPP
#include "ActiveAgent.hpp"
#include "Egg.hpp"
#include <stdlib.h>     /* srand, rand */
#include <time.h> 
#define MOVEINHISROOM 6
class Queen: public ActiveAgent{
	public:
		Queen(int,int,int,int,int,int,Square*,int=8,int=2);
		Queen operator=(const Queen&)=delete;
		Queen(const Queen&)=delete;
		bool haveEgg();
		int getNewGeneration();
		virtual void act()override;
		virtual bool isReady();
		~Queen()=default;
	protected:
		int _freqEgg;
		bool _inHisRoom;
		int _newGeneration;
		int _nbrEgg,_maxAlea;
		int _stayInPlace;
		virtual void display()override;
		virtual void stayInRoom();
		virtual void layEgg();
		virtual bool moveSquare(Square*) override;
};

#endif /* QUEEN_HPP */