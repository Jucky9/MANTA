#include "Map.hpp"
#include <iostream>
#include <fstream>
#include <string>

Map::Map(std::string mapFileName,float outS,float inS,bool build ) : _mapFileName(mapFileName),_mapSquare(),_spawn(),_outSide(true),STIMULINSTRONG(inS),STIMULOUTSTRONG(outS){
if(build){
  std::string line;
  std::vector<int> test;
  std::ifstream myfile (mapFileName);
  if (myfile.is_open()){
    int count = 0;
    while ( getline (myfile,line) ){
      if(count!=0){
        createObjectsByLine(line,count-1);
        }
        count++;
      }
    }
  }
}

void Map::display(){
  std::vector<std::vector<Square*>>::iterator it(_mapSquare.begin());
  std::vector<Square*>::iterator it2((*it).begin());
  while(it!=_mapSquare.end()){
    while(it2!=(*it).end()){
      (*it2)->display();
      it2++;
    }
    std::cout<<std::endl;
    it++;
    it2=(*it).begin();
  }
  std::cout<<std::endl;
}

void Map::aging(){
  std::vector<std::vector<Square*>>::iterator it(_mapSquare.begin());
  std::vector<Square*>::iterator it2((*it).begin());
  while(it!=_mapSquare.end()){
    while(it2!=(*it).end()){
      (*it2)->aging();
      it2++;
    }
    it++;
    it2=(*it).begin();
  }
}

  
void Map::createObjectsByLine(std::string line, int ligne){
  _mapSquare.push_back(std::vector<Square*>());
  line.erase(std::remove(line.begin(), line.end(), ' '),line.end());//clears the string and erases the spaces
  std::vector<Square*>::iterator itCurrent(_mapSquare.at(ligne).begin());
  std::vector<Square*>::iterator itPrevious;
  if (ligne>0){
    itPrevious= _mapSquare.at(ligne-1).begin();
  }
  for (unsigned int i =0; i<line.size(); i++){ 
    switch(line[i]){
      case WALL:{
        itCurrent=_mapSquare.at(ligne).insert(itCurrent,new Square(new Wall(false),ligne,i,false,(_outSide ? STIMULOUTSTRONG : STIMULINSTRONG )));
        break;
      }
      case SEED:{
        itCurrent=_mapSquare.at(ligne).insert(itCurrent,new Square(new Seed(1,true,2),ligne,i,true,(_outSide ? STIMULOUTSTRONG:STIMULINSTRONG)));
        break;
      }
      case GROUND:{
        _outSide=false;
        itCurrent=_mapSquare.at(ligne).insert(itCurrent,new Square(new Ground(false),ligne,i,false,(_outSide ? STIMULOUTSTRONG:STIMULINSTRONG)));
        break;
      }
      case LEEF:{
        itCurrent=_mapSquare.at(ligne).insert(itCurrent,new Square(new Leef(3,true,5),ligne,i,true,(_outSide ? STIMULOUTSTRONG:STIMULINSTRONG)));
        break;
      }
      default :{
        itCurrent=_mapSquare.at(ligne).insert(itCurrent,new Square(ligne,i,true,(_outSide ? STIMULOUTSTRONG:STIMULINSTRONG)));
        if (line[i]==SPAWN){
          _spawn.push_back(*itCurrent);
        }
        break;
      }
    }
    if(ligne){
      if(i){
        (*itCurrent)->addNeighbours(*(itPrevious-1),7);/*diagonal gauche haut*/
        (*(itPrevious-1))->addNeighbours(*(itCurrent),3);/*diagonal droit bas*/
      }

      (*itCurrent)->addNeighbours(*(itPrevious),0);/*haut*/
      (*itPrevious)->addNeighbours(*(itCurrent),4);/*bas*/

      if(i+1!=line.size()){
        (*itCurrent)->addNeighbours(*(itPrevious+1),1);/*diagonal haut droit*/
        (*(itPrevious+1))->addNeighbours(*(itCurrent),5);/*diagonal bas gauch*/
      }
    }
    if(i){
      (*itCurrent)->addNeighbours(*(itCurrent-1),6);/*gauche*/
      (*(itCurrent-1))->addNeighbours(*itCurrent,2);/*droite*/
    }
    itCurrent++;
    if (ligne>0){
      itPrevious++;
    }
  }
}

Square* Map::getSquare(){
  return _mapSquare.at(20).at(20);
}

void Map::linkObject(){
  std::vector<std::vector<Square*>>::iterator it(_mapSquare.begin());
  std::vector<Square*>::iterator it2((*it).begin());

  while(it!=_mapSquare.end()){
    while(it2!=(*it).end()){
      (*it2)->linkObject();
      it2++;
    }

    it++;
    it2=(*it).begin();
  }
}


 std::vector<Square*> Map::getSpawnPos(){
  return _spawn;
}
Map::~Map(){
   while(_mapSquare.begin()!=_mapSquare.end()){
      while((*_mapSquare.begin()).begin()!=(*_mapSquare.begin()).end()){
        delete (*(*_mapSquare.begin()).begin());
        (*_mapSquare.begin()).erase((*_mapSquare.begin()).begin());
      }
      _mapSquare.erase(_mapSquare.begin());
   }
}