#ifndef ANTGUI_HPP
#define ANTGUI_HPP

#include <QApplication>
#include <QGridLayout>
#include <QLabel>
#include <vector>
#include <QMainWindow>
#include <QPixmap>
#include <QPropertyAnimation>
#include <QTransform>
#include <iostream>
#include "ObjectGUI.hpp"
#include "Ant.hpp"
#include "SquareGUI.hpp"
#include <math.h>
#include <unistd.h>
#define PI 3.14159265358979323846
#define DURATION 100
#define SIZE 15


class AntGUI: public ObjectGUI,public Ant {
	Q_OBJECT
	public:
		AntGUI(QGridLayout*,int,int,int,int,int,Square*,std::string,int,int,int);
		virtual ~AntGUI()=default;
		virtual bool isReady()override;
	private:
		QPropertyAnimation *anim;
		QGridLayout *grid;
		bool _annimeDone;
		bool _putObject;
	protected:
		virtual bool moveSquare(Square*)override;
		virtual void takeObject(Object*)override;
		virtual void putObject(Square*)override;
		virtual void feed(AgentEater*)override;
		virtual void death()override;
		virtual void deletFood(Object*)override;
	public slots:
		void moveSlot(Square*);
		void endAnimation();
		void takeObjectSlot(Object*);
		void normalSkinSlot();
		void deathSlot();
		void destructionSlot(Object*);
	signals:
		void moveSignal(Square*);
		void takeObjectSignal(Object*);
		void normalSkinSignal();
		void deathSignal();
		void destruction(Object*);
};

#endif
