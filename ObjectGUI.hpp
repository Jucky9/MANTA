#ifndef OBJECTGUI_h
#define OBJECTGUI_h

#include <QObject>
#include <QString>
#include <QPixmap>
#include "ClickableLabel.hpp"
#include <mutex>          // std::mutex

class ObjectGUI: public QObject{
	Q_OBJECT
public:
	ObjectGUI(QString);
	virtual ~ObjectGUI();
	virtual QLabel* getLabel();
	void deleteLabel();
	QString getPixname();
	void normalPic();
protected:
	QLabel *_label;
	QString _namePix;
	QPixmap _pic;
	std::mutex _mtx;

};



#endif