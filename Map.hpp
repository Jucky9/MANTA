#ifndef MAP_H
#define MAP_H
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <typeinfo>
#include <memory>


#include "Square.hpp"
#include "Square.hpp"
#include "Leef.hpp"
#include "Wall.hpp"
#include "Seed.hpp"
#include "Ground.hpp"
#include "Stimuli.hpp"



#define WALL '0'
#define SEED '2'
#define EGGROOM '3'
#define GROUND '7'
#define LEEF '8'
#define SPAWN '9'



class Map{
	
	public:
		Map(std::string,float ,float,bool=true);
		void display();
		Square* getSquare();
		void linkObject();
		void aging();
		std::vector<Square*> getSpawnPos();
		Map(const Map &)=default;
		virtual ~Map();
	protected:
		std::string _mapFileName;
		std::vector<std::vector<Square*>> _mapSquare;
		std::vector<Square*> _spawn;
		virtual void createObjectsByLine(std::string, int);
		void addNeighboursS(Square*,int);
		bool _outSide;
		float STIMULINSTRONG;
		float STIMULOUTSTRONG;
};

#endif