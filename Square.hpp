#ifndef SQUARE_H
#define SQUARE_H

class Trace;
#include "Stimuli.hpp"
#include "Object.hpp"
#include <memory>
#include <iostream>
#include <vector>
#include <algorithm>

#define MAXWEIGHT 54

class Square {
	public:
		std::vector<Object*> getObjectOnSquare();
		Square* getRightNeighbor(); //Voisin du droite
		Square* getUpNeighbor(); // Voisin du dessus
		Square* getBackNeighbor(); //Voisin du dessous
		Square* getLeftNeighbor(); // Voisin de gauche
		Square* getUpRightNeighbor(); // Voisin de diagonal : en haut à droite
		Square* getUpLeftNeighbor(); // Voisin de diagonal : en haut à gaucge
		Square* getBackLeftNeighbor(); // Voisin de diagonal : en bas à gauche
		Square* getBackRightNeighbor(); // Voisin de diagonal : en bas à droite
		Square* getNeighbor(int);

		virtual bool addObjectOnSquare(Object *);
		virtual void removeObjectOnSquare(Object*);
		void addNeighbours(Square*,int);
		bool canGoIn(int);
		void addTrace(Trace*);
		void removeTrace( Trace*);
		Trace* getTrace();
		bool getIsCheck();
		void setIsCheck(bool);
		void aging();
		Square()=default;
		Square(Object *,int,int,bool,float);
		Square(int,int,bool,float);
		Square(const Square&)=delete;
		void display();
		void addStimuli(std::shared_ptr<Stimuli>);
		void removeStimuli(Stimuli*);
		std::shared_ptr<Stimuli> getStimuli(int);
		void endStimuli(Stimuli*);
		int getStimuliSize();
		void linkObject();
		int getPosI();
		int getPosJ();
		bool haveStymuliType(int);
		float getStimuliPower(int);
		bool objectCanBeAdd(Object*);
		bool objectCanBeAdd(int);
		virtual ~Square();
		bool operator==(const Square& S){return this==&S;}
		bool operator!=(const Square &S){return this!=&S;}
		Square& operator =(const Square &S){_trace = S._trace;_objectOnSquare = S._objectOnSquare;_neighbourSquare=S._neighbourSquare;_posI = S._posI; _posJ = S._posJ; _typeSquare = S._typeSquare;return *this;}
		float getSimuliResi();
		bool getType();
		void removeWeight(int);
	private:
		Trace* _trace;
		std::vector<std::shared_ptr<Stimuli>> _stimuli;
		std::vector<Object*> _objectOnSquare;
		std::vector<Square*> _neighbourSquare;
		int _posI;
		int _posJ;
		bool _typeSquare;
		bool _isCheck;
		int _totalWeight;
		float _stimuliResi;
};

#endif
