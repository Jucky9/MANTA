#ifndef MENUGUI_HPP
#define MENUGUI_HPP


#include <QApplication>

#include <QWidget>
#include <QTabWidget>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include <QRadioButton>
#include <QGroupBox>
#include <QGridLayout>
#include <QLayout>
#include <iostream>
#include <QMessageBox>
#include <QDesktopWidget>
#include <QMainWindow>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QMouseEvent>
#include "MapGUI.hpp"
#include "WorldGUI.hpp"
#include "MyThread.hpp"
#include <vector>



class MenuGui : public QObject{

	Q_OBJECT

	public:
		MenuGui();
        MenuGui(int);
		~MenuGui();
		int launch_menu();
        int launch_simulation();
        void first_Spin(QGridLayout *);
        void second_Spin(QGridLayout *);
        void third_Spin(QGridLayout *);
        //void fourth_Spin(QGridLayout *);
        void normal_Trace_Laps_Spin(QGridLayout *);
        void seventh_Spin(QGridLayout *);
        void eigth_Spin(QGridLayout *);
        void outsideSpreding(QGridLayout *);
        void nine_Spin(QGridLayout *);
        void tenth_Spin(QGridLayout *);
        void eleventh_Spin(QGridLayout *);
        void twelth_Spin(QGridLayout *);
        void thirteenth_Spin(QGridLayout *);
        void fourteenth_Spin(QGridLayout *);
        void fifteenth_Spin(QGridLayout *);
        void sixteenth_Spin(QGridLayout *);
        void seventeenth_Spin(QGridLayout *);
        void eigtheenth_Spin(QGridLayout *);
        void nineteenth_Spin(QGridLayout *);
        void twentieth_Spin(QGridLayout *);
        void hungry_Q_Resistance_Spin(QGridLayout *);
        void lay_Frequence_Spin(QGridLayout *);
        void egg_Number_Spin(QGridLayout *);
        void lapTime_Before_Hatching_Spin(QGridLayout *);
        void lapTime_Before_LarvaEvo_Spin(QGridLayout *);
        void larva_Frequence_Hungry_Spin(QGridLayout *);
        void larva_Hungry_Resistance_Spin(QGridLayout *);
        void evolution_If_Hungry_Spin(QGridLayout *, int, int);
        void cocoon_Hatching_Time_Spin(QGridLayout *);
        void first_groupe(QGridLayout *, int, int );
        void second_groupe(QGridLayout *, int, int );
        void third_groupe (QGridLayout *, int, int );
        void fourth_groupe (QGridLayout *, int, int );
        void fifth_groupe (QGridLayout *, int, int );
        //void sixth_groupe (QGridLayout *, int, int );
        //void seventh_groupe (QGridLayout *, int, int );
        void eigth_groupe (QGridLayout *, int, int );
        void nineth_groupe (QGridLayout *, int, int );
        void tenth_groupe (QGridLayout *, int, int );
        void eleventh_groupe (QGridLayout *, int, int );
        void twelveth (QGridLayout *, int, int );
        void thirteenth (QGridLayout *, int, int );
        void antSchoice (QGridLayout *, int, int );
        void seedSchoice (QGridLayout *, int, int );
        void leafSchoice (QGridLayout *, int, int );
        void grassSchoice (QGridLayout *, int, int );
        void fillList();
        void isThereAQueen(QGridLayout *, int, int,int);
        void eggs_number(QGridLayout *, int, int);
        void setStyleSheetForRadioButtons(std::vector<QRadioButton *>);
        std::string getMapObjectImage(QRadioButton * [], std::string []);
	public slots:
		void get_user_input();
	private:
        int _MenuType;
        int height;
        int width;
        int antNumber;
        float stimuliSpreading;
        int antIntel;
        int liveTime;
        int larvaTime;
        int eggTime;
        int turnLayQueen;
        int _queenLifetime;
        int _eggsNumber;
        bool _isThereAQueen;
        int _larvasNumber;
        int _hungryResistance;
        float stimuliSpreading2;
        int seedNutrition;
        int leafNutrition;
        int antFrequenceMovement;
        int queenIntel;
        int _queenhungryResistance;
        int _queenHungry;
        int _antFrequenceHungry;
        int queenFrequenceMovement;
        int  pupaNumber;
        int pupaTime;
        bool evolutionIfHungry;
        int larvaFrequenceHungry;
        int _hungryResistanceLarva;
        int foodTraceLifetime;
        int ignoringTrace;
        int normalTraceLaps;

        
        WorldGUI *_world;
        MyThread *a;
        
        QMainWindow* mainWindow;
		QWidget* fenetre;
        QWidget* fenetre2=nullptr;

        QDesktopWidget *screen;
        QRect rect;
        std::string _map;

        QSpinBox * larva_number;
        QSpinBox * ant_number;
        QSpinBox * laps_time;
        QDoubleSpinBox * StimuliSpreading_O;
        QDoubleSpinBox * StimuliSpreading_I;
        QSpinBox * seedQuantity;
        QSpinBox * leafQuantity;
        QSpinBox * antIntelligence;
        QSpinBox * ant_Frequence_Movement;
        QSpinBox * ant_Frequence_Hungry;
        QSpinBox * hungry_Resistance;
        QSpinBox * ant_Lifetime;
        QSpinBox * food_Trace_Lifetime;
        QSpinBox * ignoring_Trace;
        QSpinBox * queen_Intelligence;
        QSpinBox * queen_Frequence_Hungry;
        QSpinBox * queen_Frequence_movement;
        QSpinBox * hungry_Resistance_Queen;
        QSpinBox * lay_Frequence;
        QSpinBox * eggs_Number;
        QSpinBox * before_Hatching;
        QSpinBox * before_LarvaEvo;
        QSpinBox * larva_Frequence_Hungry;
        QSpinBox * larva_Hungry_Resistance;
        QSpinBox * evolution_If_Hungry;
        QSpinBox * cocoon_Hatching_Time;
        QSpinBox * normal_Trace_Laps;
        QSpinBox * number_Of_Puppa;
        QPushButton *validate;

		QGroupBox *groupBox1;
		QRadioButton *larva_number_high ;
		QRadioButton *larva_number_normal;
		QRadioButton *larva_number_low;
		QVBoxLayout *vbox1;

		QGroupBox *groupBox2;
    	QRadioButton *ant_number_high;
   	 	QRadioButton *ant_number_normal;
    	QRadioButton *ant_number_low;
    	QVBoxLayout *vbox2;

    	QGroupBox *groupBox3;
    	QRadioButton *Intelligence_high;
    	QRadioButton *Intelligence_normal;
    	QRadioButton *Intelligence_low;
    	QVBoxLayout *vbox3;

        QGroupBox *groupBox14;
        QRadioButton *Hungry_high;
        QRadioButton *Hungry_normal;
        QRadioButton *Hungry_low;
        QVBoxLayout *vbox14;

    	QGroupBox *groupBox4;
    	QRadioButton *food_percent_high;
    	QRadioButton *food_percent_normal;
    	QRadioButton *food_percent_low;
    	QVBoxLayout *vbox4;

    	QGroupBox *groupBox5;
    	QRadioButton *big_map;
    	QRadioButton *normal_map;
    	QRadioButton *low_map;
    	QVBoxLayout *vbox5;

    	QGroupBox *groupBox6;
    	QRadioButton *diseace_probability_high;
    	QRadioButton *diseace_probability_normal;
    	QRadioButton *diseace_probability_low;
    	QVBoxLayout *vbox6;

    	QGroupBox *groupBox7;
    	QRadioButton *disease_spreading_high;
    	QRadioButton *disease_spreading_normal;
    	QRadioButton *disease_spreading_low;
    	QVBoxLayout *vbox7;

    	QGroupBox *groupBox8;
    	QRadioButton *queenLifetime_high;
    	QRadioButton *queenLifetime_normal;
    	QRadioButton *queenLifetime_low;
    	QVBoxLayout *vbox8;

    	QGroupBox *groupBox9;
    	QRadioButton *stimuli_spreading_high;
    	QRadioButton *stimuli_spreading_normal;
    	QRadioButton *stimuli_spreading_low;
    	QVBoxLayout *vbox9;

    	QGroupBox *groupBox10;
    	QRadioButton *ant_lifetime_high;
    	QRadioButton *ant_lifetime_normal;
    	QRadioButton *ant_lifetime_low;
    	QVBoxLayout *vbox10;

    	QGroupBox *groupBox11;
    	QRadioButton *larva_state_duration_high;
    	QRadioButton *larva_state_duration_normal;
    	QRadioButton *larva_state_duration_low;
    	QVBoxLayout *vbox11;

        QGroupBox *groupBox12;
    	QRadioButton *eggs_state_duration_high;
    	QRadioButton *eggs_state_duration_normal;
    	QRadioButton *eggs_state_duration_low ;
    	QVBoxLayout *vbox12 ;

        QGroupBox *groupBox13;
    	QRadioButton *qweens_fecondity_high;
    	QRadioButton *qweens_fecondity_normal;
    	QRadioButton *qweens_fecondity_low;
    	QVBoxLayout *vbox13;

        QGroupBox *groupBoxQ;
        QRadioButton *isThereAQueen_yes;
        QRadioButton *isThereAQueen_no;
        QVBoxLayout *vboxQ;

        QGroupBox *groupBoxEgg;
        QRadioButton *eggs_number_high;
        QRadioButton *eggs_number_normal;
        QRadioButton *eggs_number_low;
        QVBoxLayout *vboxEgg;

        QGroupBox *groupBoxEvo;
        QRadioButton *evolution_If_Hungry_Y;
        QRadioButton *evolution_If_Hungry_N;
        QVBoxLayout *vboxEvo;

    	QRadioButton *antSchoice1;
		QRadioButton *antSchoice2;
		QRadioButton *antSchoice3;
		QRadioButton *antSchoice4;

		QRadioButton *seedSchoice1;
		QRadioButton *seedSchoice2;
		QRadioButton *seedSchoice3;
		QRadioButton *seedSchoice4;

		QRadioButton *leafSchoice1;
		QRadioButton *leafSchoice2;
		QRadioButton *leafSchoice3;
		QRadioButton *leafSchoice4;

		QRadioButton *grassSchoice1;
		QRadioButton *grassSchoice2;
		QRadioButton *grassSchoice3;

        
        QRadioButton *antsChoiceButton [4];
        QRadioButton *seedsChoiceButton [4];
        QRadioButton *leafsChoiceButton [4];
        QRadioButton *grassChoiceButton [3];

        std::string antsChoiceimages [4];
        std::string seedsChoiceimages [4];
        std::string leafsChoiceimages [4];
};



void foo(World*);

#endif
