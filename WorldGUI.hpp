#ifndef WORLDGUI_HPP
#define WORLDGUI_HPP

#include "EggGUI.hpp"
#include "LarvaGUI.hpp"
#include "PupaGUI.hpp"
#include "AntGUI.hpp"
#include "LeafGUI.hpp"
#include "SeedGUI.hpp"
#include "MapGUI.hpp"
#include "QueenGUI.hpp"
#include "World.hpp"
#include <mutex>          // std::mutex

class WorldGUI: public QObject, public World{
	Q_OBJECT
	public:
		WorldGUI(MapGUI*,std::string,std::string,std::string,std::string,int, int, int, int, int,int,int,bool,int,int,int,int,int);
		WorldGUI(const WorldGUI&)=delete;
		WorldGUI operator=(const WorldGUI&)=delete;
		virtual void createAnt(int,int,int,int,int,Square*,int,int,int)override;
		virtual void createEgg(int,Square*)override;
		virtual void createLarva(int,int,int,Square*,bool)override;
		virtual void createPupa(int,Square*)override;
		virtual void createQueen(int,int,int,int,int,int,Square*,std::string,int=8,int=2)override;
		virtual void createLeaf(int weight,bool isMovable,int nbrPortion,std::string pictureName,Square*);
		virtual void createSeed(int weight,bool isMovable,int nbrPortion,std::string pictureName,Square*);
		void stop();
		~WorldGUI()=default;
	private:
		MapGUI *_map;
		std::mutex _mtx;
		std::string _antPic;
		std::string _eggPic;
		std::string _larvaPic;
		std::string _pupaPic;
	protected:
		virtual void oneTurn(std::mt19937)override;
		virtual void deleteObj(Object*)override;
	public slots:
		void createAntSlot(int,int,int,int,int,Square*,int,int,int);
		void createEggSlot(int,Square*);
		void createLarvaSlot(int,int,int,Square*,bool);
		void createPupaSlot(int,Square*);
		void createQueenSlot(int,int,int,int,int,int,Square*,std::string,int,int);
		void createLeafSlot(int,bool,int,std::string,Square*);
		void createSeedSlot(int,bool,int,std::string,Square*);
		void destructionSlot(Object*);
	signals:
		void createAntSignal(int,int,int,int,int,Square*,int,int,int);
		void createEggSignal(int,Square*);
		void createLarvaSignal(int,int,int,Square*,bool);
		void createPupaSignal(int,Square*);
		void createQueenSignal(int,int,int,int,int,int,Square*,std::string,int,int);
		void createLeafSignal(int,bool,int,std::string,Square*);
	 	void createSeedSignal(int,bool,int,std::string,Square*);
	 	void destruction(Object*);

};




#endif /* WORLDGUI_HPP */