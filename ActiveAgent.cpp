#include "ActiveAgent.hpp"

using iterator = std::vector<int>::iterator;

ActiveAgent::ActiveAgent(int weight,int range,int turnHungry, int nbrDeathEating,int frequenMove ,int turnDeath,int stimuliType): AgentEater(weight,range,turnHungry,nbrDeathEating,stimuliType),_frequenMove(frequenMove)
						,_goal(0),_turnDeath(turnDeath),_lastSquare(nullptr),_movePos(),_path() {
	for (int i=0; i<8; ++i) _movePos.push_back(i);
}

ActiveAgent::ActiveAgent(int weight,int range,int turnHungry, int nbrDeathEating,int frequenMove,int turnDeath, Square* Pos ,int stimuliType): AgentEater(weight,range,turnHungry,nbrDeathEating,Pos,stimuliType),_frequenMove(frequenMove),_goal(0)
						,_turnDeath(turnDeath),_lastSquare(nullptr),_movePos(),_path() {
	for (int i=0; i<8; ++i) _movePos.push_back(i);
}

void ActiveAgent::randomMove(){
	std::shuffle ( _movePos.begin(), _movePos.end(),_g);
	bool move(false);
	iterator it(_movePos.begin());
	Square* next(nullptr);
	while(!move and it!=_movePos.end()){
		switch(*it){
			case UP:
				next=_myPosition->getUpNeighbor();
				if(moveIsOk(next)){
						moveSquare(next);
						move=true;
					}
				break;
			case UPR:
				next=_myPosition->getUpRightNeighbor();
				if(moveIsOk(next)){
						moveSquare(next);
						move=true;
				}
				break;
			case RIGHT:
				next=_myPosition->getRightNeighbor();
				if(moveIsOk(next)){
						moveSquare(next);
						move=true;
				}
				break;
			case DOWNR:
				next=_myPosition->getBackRightNeighbor();
				if(moveIsOk(next)){
						moveSquare(next);
						move=true;
				}
				break;
			case DOWN:
				next=_myPosition->getBackNeighbor();
				if(moveIsOk(next)){
						moveSquare(next);
						move=true;
				}
				break;
			case DOWNL:
				next=_myPosition->getBackLeftNeighbor();
				if(moveIsOk(next)){
						moveSquare(next);
						move=true;
				}
				break;
			case LEFT:
				next=_myPosition->getLeftNeighbor();
				if(moveIsOk(next)){
						moveSquare(next);
						move=true;
				}
				break;
			case UPL:
				next=_myPosition->getUpLeftNeighbor();
				if(moveIsOk(next)){
						moveSquare(next);
						move=true;
					}
				break;
				}
				
		it++;	
	}
	if(!move){
		_lastSquare=nullptr;
	}
}


bool ActiveAgent::moveSquare(Square * next){
	if(next and _myPosition){
		if(next->addObjectOnSquare(this)){
			_myPosition->removeObjectOnSquare(this);
			_lastSquare=_myPosition;
			_myPosition=next;
			return true;
		}
	}
	return false;
}

bool ActiveAgent::moveAway(Square* next){
	if(_lastSquare){
		bool ok(next==_lastSquare);
		int i=0;
		while(!ok and i<8){
			ok=_lastSquare->getNeighbor(i)==next;
			i+=2;
		}
		return !ok;
	}
	return true;
}

bool ActiveAgent::inARoom(int size,int maxTaille){
	int currentSize(0);
	int j=0;
	bool free(true);
	if(!haveNWall(_myPosition,6)){
		std::queue<Square*> all;
		std::queue<Square*> pas;
		Square* current,*next;
		current=_myPosition;
		current->setIsCheck(true);
		all.push(current);
		pas.push(current);
		j=0;
		while(j<current->getStimuliSize()and free){
			free=current->getStimuli(j)->getRoot()==this or current->getStimuli(j)->getOrinin()!=current->getStimuli(j)->getRoot()->getPos();
			j++;

		}
		while (currentSize<maxTaille and all.size() and free){
			current=all.front();
			all.pop();
			for(int i=0;i<8;i++){
				next=current->getNeighbor(i);
				if(next and next->canGoIn(0) and !haveNWall(next,6) and !next->getIsCheck()){
					j=0;
					while(j<next->getStimuliSize() and free){
						free=next->getStimuli(j)->getRoot()==this or next->getStimuli(j)->getOrinin()!=next->getStimuli(j)->getRoot()->getPos();
						++j;

					}
					all.push(next);
					next->setIsCheck(true);
					currentSize+=1;
					pas.push(next);
				}
			}
		}
		while (pas.size()){
			current=pas.front();
			pas.pop();
			current->setIsCheck(false);
		}
	}
	return currentSize>=size and currentSize<maxTaille and free;

}

bool ActiveAgent::haveNWall(Square* current,int n){
	int i=0;
	Square* next;
	for(int j=0;j<8;j++){
		next=current->getNeighbor(j);
		if(!next or !next->canGoIn(_weight)){
			i++;
		}
	}
	return i>=n;
}

bool ActiveAgent::moveIsOk(Square* next){
	return ( next and next->canGoIn(_weight) and moveAway(next));

}
Square* ActiveAgent::findWallOfRoom(){
	std::queue<Square*> all;
	std::queue<Square*> pas;
	pas.push(_myPosition);
	all.push(_myPosition);
	_myPosition->setIsCheck(true);
	bool find(false);
	Square *goal(nullptr),*current(nullptr),*next(nullptr);
	while (all.size() and !find){
		current=all.front();
		all.pop();
		for(int i=0;i<8;i++){
			next=current->getNeighbor(i);
			if(next and next->canGoIn(_weight) and !haveNWall(next,5)){
				if(haveNWall(next,3)){
					find=true;
					goal=next;
				}
				else{
					next->setIsCheck(true);
					all.push(next);
					pas.push(next);
				}
			}	
		}
	}
	while (pas.size()){
		current=pas.front();
		pas.pop();
		current->setIsCheck(false);
	}
	return goal;

}
void ActiveAgent::goToWall(){
	if(!_path.size()){
		Square* wall(findWallOfRoom());
		if(wall){
			Astar temp(_myPosition,wall,_weight);
			_path=temp.getPath();
		}
	}
	goToObjective();
}
void ActiveAgent::goToObjective(){
	if(_path.size()){
		moveSquare(*(_path.begin()));
		_path.erase(_path.begin());
	}
}
void ActiveAgent::moveAwayDoor(){
	_path.clear();
	int directionDoor(0);
	int i=0;
	Square* temp(nullptr),*next(nullptr);
	for(int i=0;i<8;i++){
		temp=_myPosition->getNeighbor(i);
		if(temp and temp->canGoIn(_weight) and haveNWall(temp,6)){
			directionDoor=i;
		}
	}
	temp=_myPosition;
	for(int j=0;j<4;j++){
		next=nullptr;
		i=0;
		while(!next and i!=2){
			next=temp->getNeighbor((directionDoor+4+i)%8);
			if(!(next and next->canGoIn(_weight)))
				next=nullptr;
			switch(i){
				case 0:
					i=-1;
					break;
				case -1:
					i=1;
					break;
				case 1:
					i=-2;
					break;
				case -2:
					i=2;
					break;
			}
		}
		if(next){
			_path.push_back(next);
			temp=next;
		}
	}
}
bool ActiveAgent::InBack(Square* tryInBack,int typeLastMove){
	bool back(false);
	if(typeLastMove!=-1){
		switch(typeLastMove){
			case 0:
				back=((_myPosition->getPosI()+_myPosition->getPosJ())<=(tryInBack->getPosI()+tryInBack->getPosJ())and  (tryInBack->getPosJ()-_myPosition->getPosJ())<=(tryInBack->getPosI()-_myPosition->getPosI()));
				break;
			case 2:
				back=((_myPosition->getPosI()+_myPosition->getPosJ())>=(tryInBack->getPosI()+tryInBack->getPosJ()) and  (tryInBack->getPosJ()-_myPosition->getPosJ())<=(tryInBack->getPosI()-_myPosition->getPosI()));
				break;
			case 4:
				back=((_myPosition->getPosI()+_myPosition->getPosJ())>=(tryInBack->getPosI()+tryInBack->getPosJ()) and  (tryInBack->getPosJ()-_myPosition->getPosJ())>=(tryInBack->getPosI()-_myPosition->getPosI()));
				break;
			case 6:
				back=((_myPosition->getPosI()+_myPosition->getPosJ())<=(tryInBack->getPosI()+tryInBack->getPosJ()) and  (tryInBack->getPosJ()-_myPosition->getPosJ())>=(tryInBack->getPosI()-_myPosition->getPosI()));
				break;
			case 1:
				back=(_myPosition->getPosI()<=tryInBack->getPosI() and _myPosition->getPosJ()>=tryInBack->getPosJ());
				break;
			case 3:
				back=(_myPosition->getPosI()>=tryInBack->getPosI() and _myPosition->getPosJ()>=tryInBack->getPosJ());
				break;
			case 5:
				back=(_myPosition->getPosI()>=tryInBack->getPosI() and _myPosition->getPosJ()<=tryInBack->getPosJ());
				break;
			case 7:
				back=(_myPosition->getPosI()<=tryInBack->getPosI() and _myPosition->getPosJ()<=tryInBack->getPosJ());
				break;
		}
	}
	return back;
}

bool ActiveAgent::InFront(Square* tryInFront,int typeLastMove){
	if(typeLastMove!=-1){
		return(InBack(tryInFront,(typeLastMove+4)%8));
	}
	return true;

}


int ActiveAgent::typeLastMove(){
	int count(-1);
	if(_lastSquare){
		Square*next=_lastSquare->getNeighbor(count);
		while (count<8 and !(next and next==_myPosition)){
			count++;
			next=_lastSquare->getNeighbor(count);
		}
	}
	return count;
}

bool ActiveAgent::pathInBAck(Square* next,int typeMove){
	bool back(false);
	if(next and typeMove!=-1){
		Astar temp(_myPosition,next,_weight);
		std::vector<Square*> tempPath=temp.getPath();
		while(tempPath.size() and !back){
			back=InBack(tempPath.at(0),typeMove);
			tempPath.erase(tempPath.begin());
		}
	}
	return back;
}


std::random_device ActiveAgent::_rd;
std::mt19937 ActiveAgent::_g(ActiveAgent::_rd());