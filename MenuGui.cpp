#include "MenuGui.hpp"


MenuGui::MenuGui () :mainWindow(NULL),fenetre(NULL),fenetre2(NULL)
{
    mainWindow = new QMainWindow();
    screen = new QDesktopWidget();
    rect = screen->screenGeometry();
    mainWindow->setGeometry(rect);
    mainWindow->show();
    
}
MenuGui::MenuGui (int menutype) :mainWindow(NULL),fenetre(NULL),fenetre2(NULL)
{
    mainWindow = new QMainWindow();
    screen = new QDesktopWidget();
    rect = screen->screenGeometry();
    mainWindow->setGeometry(rect);
    mainWindow->show();
    _MenuType = menutype;
    
}
MenuGui::~MenuGui()
{
	if(fenetre)
	{
		delete fenetre;
		fenetre = 0;
	}

}
int MenuGui::launch_menu()
{
	fenetre = new QWidget(mainWindow);
    fenetre->setGeometry(0,0,rect.width(),rect.height());
    QTabWidget *onglets = new QTabWidget(fenetre);

    onglets->setStyleSheet("QTabWidget::tab-bar{left:0px; }\n"
	"QTabBar::tab{height:40px; width:105px; color:red;font: 9pt}\n");
    QWidget *page1 = new QWidget (onglets);
    onglets->setFixedSize(rect.width()-70,rect.height()-40);

    QLabel *fond_ecran = new QLabel(page1);
    //fond_ecran->setSizePolicy(QSizePolicy::Maximum);//setGeometry(0,0,500,500);

    //QWidget *widget = new QWidget;
	//QLabel *label = new QLabel("Simulation");
	//layout->addWidget(label);

	
	

    fond_ecran->setPixmap(QPixmap("3.jpg"));

    fond_ecran -> setGeometry(0,0,rect.width(),rect.height());
	QGridLayout *grille1 = new QGridLayout(page1);
	
    if(_MenuType==1)
       { 
        QLabel *titre =  new QLabel("<h1 style='font-family:arial; color:white;'> Welcome To The Manta Project </h1><h3 style='font-family:arial; color:white;' >The Project That Simulate Ant's behaviour ...</h3>");
        grille1 -> addWidget (titre, 0,0);
        //non-scientifique
        first_groupe(grille1,1,0);
        second_groupe(grille1,1,1);
        third_groupe (grille1,1,2);
        //fourth_groupe(grille1, 1,3);
        fifth_groupe (grille1,2,1);
        eigth_groupe (grille1,3,1);
        nineth_groupe (grille1,3,2);
        tenth_groupe (grille1,1,3);
        eleventh_groupe (grille1,2,2);
        twelveth (grille1,3,0);
        thirteenth (grille1,2,0);
        isThereAQueen(grille1, 3, 3,1);
        eggs_number(grille1,2,3);
        validate = new QPushButton(" > Simulate ");
        validate->setDefault(true);
        validate->setAutoDefault(false);
        validate->setStyleSheet("background-color: #4CAF50; color: white; font-size:35px; border: #4CAF50; padding: 40px; ");
        validate->setCursor(Qt::PointingHandCursor);
        grille1->addWidget(validate, 4, 3);
        QObject::connect(validate, SIGNAL(clicked()), this, SLOT(get_user_input()));
        }

    else 
        {

        fond_ecran->setPixmap(QPixmap("2.jpg"));
        fond_ecran -> setGeometry(0,0,rect.width(),rect.height());
        QLabel *titre =  new QLabel("<h1 style='font-family:arial; color:black;'> Welcome To The Manta Project - Scientific mode </h1>");
        grille1 -> addWidget (titre, 0,0);
        //scientifique
        first_Spin(grille1);
        second_Spin(grille1);
        //third_Spin(grille1);
        seventh_Spin(grille1);
        eigth_Spin(grille1);
        outsideSpreding(grille1);
        nine_Spin(grille1);
        tenth_Spin(grille1);
        eleventh_Spin(grille1);
        twelth_Spin(grille1);
        thirteenth_Spin(grille1);
        fourteenth_Spin(grille1);
        fifteenth_Spin(grille1);
        sixteenth_Spin(grille1);
        seventeenth_Spin(grille1);
        eigtheenth_Spin(grille1);
        nineteenth_Spin(grille1);
        twentieth_Spin(grille1);
        hungry_Q_Resistance_Spin(grille1);
        lay_Frequence_Spin(grille1);
        egg_Number_Spin(grille1);
        lapTime_Before_Hatching_Spin(grille1);
        lapTime_Before_LarvaEvo_Spin(grille1);
        larva_Frequence_Hungry_Spin(grille1);
        larva_Hungry_Resistance_Spin(grille1);
        evolution_If_Hungry_Spin(grille1, 2, 3);
        cocoon_Hatching_Time_Spin(grille1);
        isThereAQueen(grille1,15,3,6);
        normal_Trace_Laps_Spin(grille1);
        validate = new QPushButton(" > Simulate ");
        validate->setStyleSheet("background-color: #4CAF50; color: white; font-size:35px; border: #4CAF50; padding: 40px; ");
        validate->setCursor(Qt::PointingHandCursor);
        grille1->addWidget(validate, 32, 3, 6,1);
        QObject::connect(validate, SIGNAL(clicked()), this, SLOT(get_user_input()));
        }

    page1->setLayout(grille1);
    QWidget *page2 = new QWidget (fenetre);
    page2->setStyleSheet("background: url(QTabs.jpg);") ;
                        //"background-repeat: no-repeat;");


    QGridLayout *grille2 = new QGridLayout(page2);

    QLabel *text_page2_2 = new QLabel(
    									"<h1 style='font-family:arial; color:green; text-align:justify;'> About The Project </h1>"
    									"<p style='font-family:arial; text-align:justify; width:100px;'>"
    									"<justify>Our interest is the simulation of the evolution of complex systems "
    	                                "where interactions performed between several individuals" 
    	                                "at the micro levelare responsible of measurable general situations "
    	                                "observed at the macro level. <br /><br /> <b>When the situation is too complex to "
    	                                "study it is essential to recreate an artificial universe in "
    	                                "which experiments can be conducted in a simulated laboratory where all "
    	                                "parameters arecontrolled precisely.</b> <br /><br /> In this simulation we describe a  general "
    	                                "model of simulation ofcomplex societies based on the simulation of the behavior " 
    	                                "of its individuals.  We propose then to illustrate this model of simulation with "
    	                                "the modelling of an ant nest.  One aim of this work is to understand the mechanisms "
    	                                "of sociogenesis through simple ontogenetic factors.</justify></p>");
    text_page2_2->setWordWrap(true);
	grille2 -> addWidget (text_page2_2, 0, 0);
    grille2->setAlignment(Qt::AlignTop);
    QWidget *page3 = new QWidget (fenetre);
    page3->setStyleSheet("background: white   ;") ;
    QLabel *fond_ecran1 = new QLabel(page3);
    fond_ecran1->setPixmap(QPixmap("ULB.jpg"));

    fond_ecran1 -> setGeometry(630,0,1200,1200);
    QGridLayout *grille3 = new QGridLayout(page3);

    QLabel *text_page3_3 = new QLabel(
    									"<div class='jumbotron'><h1 style='font-family:arial; color:green;margin-top:-50px;'> About The Developpers </h1>"
    									"<p style='font-family:arial'>"
    									"The Manta project was developped by  :"
    	                                "</p>"
    	                                "<ul>"
    	                                	"<li>DELBEKE Julien</li>"
    	                                	"<li>HULLEBROECK Nathan</li>"
    	                                	"<li>MARECAR Ansary</li>"
    	                                	"<li>RAFAELE Antonio</li>"
    	                                	"<li>RAMOS PEREZ Eduardo</li>"
                                            "<li>SEFU Kevin</li>"
    	                                "</ul>"
    	                                "<p><br />The developpement was in collaboration with ULB (Universite Libre de Bruxelles).</p></div>"
    	                                );
    text_page3_3->setWordWrap(true);
	grille3 -> addWidget (text_page3_3, 0, 0);
    grille3->setAlignment(Qt::AlignTop);

    QWidget *page4 = new QWidget (fenetre);
    QGridLayout *grille4 = new QGridLayout(page4);
    QLabel *fond_ecran3 = new QLabel(page4);

    fond_ecran3->setPixmap(QPixmap("antBG.png"));

    fond_ecran3 -> setGeometry(1550,0,1200,1200);
    antSchoice(grille4, 0, 0);
    page4->setLayout(grille4);

    QWidget *page5 = new QWidget (fenetre);
    QGridLayout *grille5 = new QGridLayout(page5);
        QLabel *fond_ecran4 = new QLabel(page5);

    fond_ecran4->setPixmap(QPixmap("antBG.png"));

    fond_ecran4 -> setGeometry(1550,0,1200,1200);
    seedSchoice(grille5, 0, 0);
    page5->setLayout(grille5);

    QWidget *page6 = new QWidget (fenetre);
    QGridLayout *grille6 = new QGridLayout(page6);
    QLabel *fond_ecran5 = new QLabel(page6);

    fond_ecran5->setPixmap(QPixmap("antBG.png"));

    fond_ecran5 -> setGeometry(1550,0,1200,1200);
    leafSchoice(grille6, 0, 0);
    page6->setLayout(grille6);

    QWidget *page7 = new QWidget (fenetre);
    QGridLayout *grille7 = new QGridLayout(page7);
    QLabel *fond_ecran7 = new QLabel(page7);
    fond_ecran7->setPixmap(QPixmap("antBG.png"));
    fond_ecran7 -> setGeometry(1550,0,1200,1200);
    grassSchoice(grille7, 0, 0);
    page7->setLayout(grille7);
    
    onglets->addTab(page1,"Simulation");
    onglets->addTab(page2, "The Project");
    onglets->addTab(page3, "Developpers");
    onglets->addTab(page4, "Ant's Choice");
    onglets->addTab(page5, "Seed's Choice");
    onglets->addTab(page6, "Leaf's Choice");
    onglets->addTab(page7, "Grass's Choice");
    fenetre->show();
    
    return 0;
}
void MenuGui::antSchoice (QGridLayout * grille1, int x, int y){

    QGroupBox *group = new QGroupBox ("Ant's category");
    antSchoice1 = new QRadioButton("Solenopsis");
    antSchoice1->setChecked(true);
    antSchoice2 = new QRadioButton("Paraponera");
    antSchoice3 = new QRadioButton("Lasius");
    antSchoice4 = new QRadioButton("Myrmecia");
    QVBoxLayout *vbox3x = new QVBoxLayout;
    vbox3x->addWidget (antSchoice1);
    vbox3x->addWidget (antSchoice2);
    vbox3x->addWidget (antSchoice3);
    vbox3x->addWidget (antSchoice4);
    group->setLayout(vbox3x);
    grille1->addWidget (group, x, y);
    QGroupBox *group2 = new QGroupBox ("Ant's Images");
    QLabel *free = new QLabel();
	free->setScaledContents(true);
	free->setStyleSheet("background:transparent ;");
	free->setPixmap(QPixmap("ant1.png"));
	free->setFixedWidth(180);
	free->setFixedHeight(170);
	QLabel *free1 = new QLabel();
	free1->setScaledContents(true);
	free1->setStyleSheet("background:transparent ;");
	free1->setPixmap(QPixmap("cat1Ant.png"));
	free1->setFixedWidth(180);
	free1->setFixedHeight(170);
	QLabel *free2 = new QLabel();
	free2->setScaledContents(true);
	free2->setStyleSheet("background:transparent ;");
	free2->setPixmap(QPixmap("cat2Ant.png"));
	free2->setFixedWidth(180);
	free2->setFixedHeight(170);
	QLabel *free3 = new QLabel();
	free3->setScaledContents(true);
	free3->setStyleSheet("background:transparent ;");
	free3->setPixmap(QPixmap("cat3Ant.png"));
	free3->setFixedWidth(180);
	free3->setFixedHeight(170);
    QVBoxLayout *vbox3x2 = new QVBoxLayout;
    vbox3x2->addWidget (free);
    vbox3x2->addWidget (free1);
    vbox3x2->addWidget (free2);
     vbox3x2->addWidget (free3);
    group2->setLayout(vbox3x2);
    grille1->addWidget (group2, 0, 1);

}
void MenuGui::seedSchoice (QGridLayout * grille1, int x, int y){
    QGroupBox *group = new QGroupBox ("Seed's category");
    seedSchoice1 = new QRadioButton("Pivot's seed");
    seedSchoice1->setChecked(true);
    seedSchoice2 = new QRadioButton("Niger's seed");
    seedSchoice3 = new QRadioButton("Quinoa's seed");
    seedSchoice4 = new QRadioButton("Amaranthe's seed");
    QVBoxLayout *vbox3x = new QVBoxLayout;
    vbox3x->addWidget (seedSchoice1);
    vbox3x->addWidget (seedSchoice2);
    vbox3x->addWidget (seedSchoice3);
    vbox3x->addWidget (seedSchoice4);
    group->setLayout(vbox3x);
    grille1->addWidget (group, x, y);
    QGroupBox *group2 = new QGroupBox ("Seed's Images");
    QLabel *free = new QLabel();
	free->setScaledContents(true);
	free->setStyleSheet("background:transparent ;");
	free->setPixmap(QPixmap("seed.png"));
	free->setFixedWidth(180);
	free->setFixedHeight(170);
	QLabel *free1 = new QLabel();
	free1->setScaledContents(true);
	free1->setStyleSheet("background:transparent ;");
	free1->setPixmap(QPixmap("seed2.png"));
	free1->setFixedWidth(180);
	free1->setFixedHeight(170);
	QLabel *free2 = new QLabel();
	free2->setScaledContents(true);
	free2->setStyleSheet("background:transparent ;");
	free2->setPixmap(QPixmap("seed3.png"));
	free2->setFixedWidth(180);
	free2->setFixedHeight(170);
	QLabel *free3 = new QLabel();
	free3->setScaledContents(true);
	free3->setStyleSheet("background:transparent ;");
	free3->setPixmap(QPixmap("seed4.png"));
	free3->setFixedWidth(180);
	free3->setFixedHeight(170);
    QVBoxLayout *vbox3x2 = new QVBoxLayout;
    vbox3x2->addWidget (free);
    vbox3x2->addWidget (free1);
    vbox3x2->addWidget (free2);
     vbox3x2->addWidget (free3);
    group2->setLayout(vbox3x2);
    grille1->addWidget (group2, 0, 1);

}
void MenuGui::leafSchoice (QGridLayout * grille1, int x, int y){

    QGroupBox *group = new QGroupBox ("Leaf's category");
    leafSchoice1 = new QRadioButton("Hortensia");
    leafSchoice2 = new QRadioButton("Hortie");
    leafSchoice3 = new QRadioButton("Salad");
    leafSchoice4 = new QRadioButton("Cresson");
    leafSchoice4->setChecked(true);
    QVBoxLayout *vbox3x = new QVBoxLayout;
    vbox3x->addWidget (leafSchoice1);
    vbox3x->addWidget (leafSchoice2);
    vbox3x->addWidget (leafSchoice3);
    vbox3x->addWidget (leafSchoice4);
    group->setLayout(vbox3x);
    grille1->addWidget (group, x, y);
    QGroupBox *group2 = new QGroupBox ("Seed's Images");
    QLabel *free = new QLabel();
	free->setScaledContents(true);
	free->setStyleSheet("background:transparent ;");
	free->setPixmap(QPixmap("leaf.png"));
	free->setFixedWidth(180);
	free->setFixedHeight(170);
	QLabel *free1 = new QLabel();
	free1->setScaledContents(true);
	free1->setStyleSheet("background:transparent ;");
	free1->setPixmap(QPixmap("leaf2.png"));
	free1->setFixedWidth(180);
	free1->setFixedHeight(170);
	QLabel *free2 = new QLabel();
	free2->setScaledContents(true);
	free2->setStyleSheet("background:transparent ;");
	free2->setPixmap(QPixmap("leaf3.png"));
	free2->setFixedWidth(180);
	free2->setFixedHeight(170);
	QLabel *free3 = new QLabel();
	free3->setScaledContents(true);
	free3->setStyleSheet("background:transparent ;");
	free3->setPixmap(QPixmap("leaf4.png"));
	free3->setFixedWidth(180);
	free3->setFixedHeight(170);
    QVBoxLayout *vbox3x2 = new QVBoxLayout;
    vbox3x2->addWidget (free);
    vbox3x2->addWidget (free1);
    vbox3x2->addWidget (free2);
     vbox3x2->addWidget (free3);
    group2->setLayout(vbox3x2);
    grille1->addWidget (group2, 0, 1);

}
void MenuGui::grassSchoice (QGridLayout *grille1, int x, int y){

    QGroupBox *group = new QGroupBox ("Grass's category");
    grassSchoice1 = new QRadioButton("Summer's grass");
    grassSchoice1->setChecked(true);
    grassSchoice2 = new QRadioButton("Autumn's grass");
    grassSchoice3 = new QRadioButton("Winter's grass");
    QVBoxLayout *vbox3x = new QVBoxLayout;
    vbox3x->addWidget (grassSchoice1);
    vbox3x->addWidget (grassSchoice2);
    vbox3x->addWidget (grassSchoice3);
    group->setLayout(vbox3x);
    grille1->addWidget (group, x, y);
    QGroupBox *group2 = new QGroupBox ("grass images");
    QLabel *free = new QLabel();
	free->setScaledContents(true);
	free->setStyleSheet("background:transparent ;");
	free->setPixmap(QPixmap("grass.jpg"));
	free->setFixedWidth(230);
	free->setFixedHeight(170);
	QLabel *free1 = new QLabel();
	free1->setScaledContents(true);
	free1->setStyleSheet("background:transparent ;");
	free1->setPixmap(QPixmap("grass1.jpg"));
	free1->setFixedWidth(230);
	free1->setFixedHeight(170);
	QLabel *free2 = new QLabel();
	free2->setScaledContents(true);
	free2->setStyleSheet("background:transparent ;");
	free2->setPixmap(QPixmap("grass2.jpg"));
	free2->setFixedWidth(230);
	free2->setFixedHeight(170);
    QVBoxLayout *vbox3x2 = new QVBoxLayout; 
    vbox3x2->addWidget (free);
    vbox3x2->addWidget (free1);
    vbox3x2->addWidget (free2);
    group2->setLayout(vbox3x2);
    grille1->addWidget (group2, 0, 1);

}
void MenuGui::setStyleSheetForRadioButtons(std::vector <QRadioButton*> buttons){
    std::vector<QRadioButton*> ::iterator it;
    for (it = buttons.begin();it!=buttons.end();++it){
        (*it)->setStyleSheet("QRadioButton { color:white;font-size:20px;background:white rgba(0, 20, 255,30);}");
    }
}
void MenuGui::first_Spin(QGridLayout *grille1)
{
    QLabel *larvaN =  new QLabel("<h3 style='font-family:arial; color:black;' >Larva Number</h3>");
    grille1 -> addWidget (larvaN, 1,0);
    larva_number = new QSpinBox;
    larva_number -> setRange(0, 300);
    larva_number ->setSingleStep(1);
    larva_number ->setValue(20);
    grille1->addWidget (larva_number, 1,1);
} 
void MenuGui::second_Spin(QGridLayout *grille1)
{
    QLabel *AntN =  new QLabel("<h3 style='font-family:arial; color:black;' >Ant Number</h3>");
    grille1 -> addWidget (AntN, 2,0);
    ant_number = new QSpinBox;
    ant_number -> setRange(0, 300);
    ant_number ->setSingleStep(1);
    ant_number ->setValue(20);
    grille1->addWidget (ant_number, 2,1);
} 


void MenuGui::seventh_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >Number of Puppas</h3>");
    grille1 -> addWidget (diseaceP, 7,0);
    number_Of_Puppa = new QSpinBox;
    number_Of_Puppa -> setRange(0, 100);
    number_Of_Puppa ->setSingleStep(1);
    number_Of_Puppa ->setValue(1);
    grille1->addWidget (number_Of_Puppa, 7,1);
}
void MenuGui::eigth_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >stimuli spreading_diminution (inside)</h3>");
    grille1 -> addWidget (diseaceP, 8,0);
    StimuliSpreading_I = new QDoubleSpinBox;
    StimuliSpreading_I -> setRange(0, 10);
    StimuliSpreading_I ->setSingleStep(0.1);
    StimuliSpreading_I ->setValue(0.1);
    grille1->addWidget (StimuliSpreading_I, 8,1);
}
void MenuGui::outsideSpreding(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >stimuli spreading_diminution (outside)</h3>");
    grille1 -> addWidget (diseaceP, 9,0);
    StimuliSpreading_O = new QDoubleSpinBox;
    StimuliSpreading_O -> setRange(0, 10);
    StimuliSpreading_O ->setSingleStep(0.1);
    StimuliSpreading_O ->setValue(1	);
    grille1->addWidget (StimuliSpreading_O, 9,1);
}
void MenuGui::nine_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >nutritious quantity of seed</h3>");
    grille1 -> addWidget (diseaceP, 10,0);
    seedQuantity = new QSpinBox;
    seedQuantity -> setRange(0, 100);
    seedQuantity ->setSingleStep(1);
    seedQuantity ->setValue(2);
    grille1->addWidget (seedQuantity, 10,1);
}
void MenuGui::tenth_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >nutritious quantity of a leaf</h3>");
    grille1 -> addWidget (diseaceP, 11,0);
    leafQuantity = new QSpinBox;
    leafQuantity -> setRange(0, 100);
    leafQuantity ->setSingleStep(1);
    leafQuantity ->setValue(3);
    grille1->addWidget (leafQuantity, 11,1);
}
//////////////////////////////////////////////
void MenuGui::eleventh_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >ant's intelligence</h3>");
    grille1 -> addWidget (diseaceP, 12,0);
    antIntelligence = new QSpinBox;
    antIntelligence -> setRange(3, 20);
    antIntelligence ->setSingleStep(1);
    antIntelligence ->setValue(3);
    grille1->addWidget (antIntelligence, 12,1);
}
void MenuGui::twelth_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >ant's frequence of movement</h3>");
    grille1 -> addWidget (diseaceP, 13,0);
    ant_Frequence_Movement = new QSpinBox;
    ant_Frequence_Movement -> setRange(0, 100);
    ant_Frequence_Movement ->setSingleStep(1);
    ant_Frequence_Movement ->setValue(1);
    grille1->addWidget (ant_Frequence_Movement, 13,1);
}
void MenuGui::thirteenth_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >ant's frequence of being hungry</h3>");
    grille1 -> addWidget (diseaceP, 14,0);
    ant_Frequence_Hungry = new QSpinBox;
    ant_Frequence_Hungry -> setRange(0, 600);
    ant_Frequence_Hungry ->setSingleStep(1);
    ant_Frequence_Hungry ->setValue(300);
    grille1->addWidget (ant_Frequence_Hungry, 14,1);
}
void MenuGui::fourteenth_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >hungry's resistance </h3>");
    grille1 -> addWidget (diseaceP, 15,0);
    hungry_Resistance = new QSpinBox;
    hungry_Resistance -> setRange(0, 20);
    hungry_Resistance ->setSingleStep(1);
    hungry_Resistance ->setValue(1);
    grille1->addWidget (hungry_Resistance, 15,1);
}
void MenuGui::fifteenth_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >ant's lifetime </h3>");
    grille1 -> addWidget (diseaceP, 16,0);
    ant_Lifetime = new QSpinBox;
    ant_Lifetime -> setRange(2, 10000);
    ant_Lifetime ->setSingleStep(1);
    ant_Lifetime ->setValue(1500);
    grille1->addWidget (ant_Lifetime, 16,1);
}
void MenuGui::sixteenth_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >food trace's lifetime </h3>");
    grille1 -> addWidget (diseaceP, 17,0);
    food_Trace_Lifetime = new QSpinBox;
    food_Trace_Lifetime -> setRange(0, 10000);
    food_Trace_Lifetime ->setSingleStep(1);
    food_Trace_Lifetime ->setValue(150);
    grille1->addWidget (food_Trace_Lifetime, 17,1);
}
void MenuGui::seventeenth_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >number of laps ignoring the trace  </h3>");
    grille1 -> addWidget (diseaceP, 19,0);
    ignoring_Trace = new QSpinBox;
    ignoring_Trace -> setRange(0, 10000);
    ignoring_Trace ->setSingleStep(1);
    ignoring_Trace ->setValue(20);
    grille1->addWidget (ignoring_Trace, 19,1);
}
void MenuGui::eigtheenth_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >queen's intelligence  </h3>");
    grille1 -> addWidget (diseaceP, 20,0);
    queen_Intelligence = new QSpinBox;
    queen_Intelligence -> setRange(0, 50);
    queen_Intelligence ->setSingleStep(1);
    queen_Intelligence ->setValue(4);
    grille1->addWidget (queen_Intelligence, 20,1);
}
void MenuGui::nineteenth_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >queen's frequence of movement  </h3>");
    grille1 -> addWidget (diseaceP, 21,0);
    queen_Frequence_movement = new QSpinBox;
    queen_Frequence_movement -> setRange(0, 8555555);
    queen_Frequence_movement ->setSingleStep(1);
    queen_Frequence_movement ->setValue(1);
    grille1->addWidget (queen_Frequence_movement, 21,1);
}
void MenuGui::twentieth_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >queen's frequence of being hungry  </h3>");
    grille1 -> addWidget (diseaceP, 22,0);
    queen_Frequence_Hungry = new QSpinBox;
    queen_Frequence_Hungry -> setRange(0, 8555555);
    queen_Frequence_Hungry ->setSingleStep(1);
    queen_Frequence_Hungry ->setValue(800);
    grille1->addWidget (queen_Frequence_Hungry, 22,1);
 }
 void MenuGui::hungry_Q_Resistance_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >hungry's resistance-queen  </h3>");
    grille1 -> addWidget (diseaceP, 23,0);
    hungry_Resistance_Queen = new QSpinBox;
    hungry_Resistance_Queen -> setRange(0, 15);
    hungry_Resistance_Queen ->setSingleStep(1);
    hungry_Resistance_Queen ->setValue(1);
    grille1->addWidget (hungry_Resistance_Queen, 23,1);
 }
  void MenuGui::lay_Frequence_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >Lay's frequence </h3>");
    grille1 -> addWidget (diseaceP, 24,0);
    lay_Frequence = new QSpinBox;
    lay_Frequence -> setRange(0, 10000);
    lay_Frequence ->setSingleStep(1);
    lay_Frequence ->setValue(450);
    grille1->addWidget (lay_Frequence, 24,1);
 }
   void MenuGui::egg_Number_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >Number of eggs </h3>");
    grille1 -> addWidget (diseaceP, 3,0);
    eggs_Number = new QSpinBox;
    eggs_Number -> setRange(0, 100);
    eggs_Number ->setSingleStep(1);
    eggs_Number ->setValue(8);
    grille1->addWidget (eggs_Number, 3,1);
 }
    void MenuGui::lapTime_Before_Hatching_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >Laps of time before hatching-egg </h3>");
    grille1 -> addWidget (diseaceP, 26,0);
    before_Hatching = new QSpinBox;
    before_Hatching -> setRange(2, 10000);
    before_Hatching ->setSingleStep(1);
    before_Hatching ->setValue(750);
    grille1->addWidget (before_Hatching, 26,1);
 }
     void MenuGui::lapTime_Before_LarvaEvo_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >Laps of time before larva's evolution </h3>");
    grille1 -> addWidget (diseaceP, 27,0);
    before_LarvaEvo = new QSpinBox;
    before_LarvaEvo -> setRange(2, 1000);
    before_LarvaEvo ->setSingleStep(1);
    before_LarvaEvo ->setValue(450);
    grille1->addWidget (before_LarvaEvo, 27,1);
 }
      void MenuGui::larva_Frequence_Hungry_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >larva's frequence of being hungry </h3>");
    grille1 -> addWidget (diseaceP, 28,0);
    larva_Frequence_Hungry = new QSpinBox;
    larva_Frequence_Hungry -> setRange(0,1000);
    larva_Frequence_Hungry ->setSingleStep(1);
    larva_Frequence_Hungry ->setValue(100);
    grille1->addWidget (larva_Frequence_Hungry, 28,1);
 }
       void MenuGui::larva_Hungry_Resistance_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >hungry's resistance-larva </h3>");
    grille1 -> addWidget (diseaceP, 29,0);
    larva_Hungry_Resistance = new QSpinBox;
    larva_Hungry_Resistance -> setRange(0, 10);
    larva_Hungry_Resistance ->setSingleStep(0.1);
    larva_Hungry_Resistance ->setValue(1);
    grille1->addWidget (larva_Hungry_Resistance, 29,1);
 }
    void MenuGui::evolution_If_Hungry_Spin(QGridLayout *grille1, int x, int y)
{
    std::vector<QRadioButton*> buttons;
    groupBoxEvo = new QGroupBox ("Evolution If Hungry??");
    evolution_If_Hungry_Y = new QRadioButton("Yes");
    evolution_If_Hungry_N = new QRadioButton("No");
    evolution_If_Hungry_Y->setChecked(true);
    buttons.push_back(evolution_If_Hungry_N);buttons.push_back(evolution_If_Hungry_Y);
    setStyleSheetForRadioButtons(buttons);
    vboxEvo = new QVBoxLayout;
    vboxEvo->addWidget (evolution_If_Hungry_Y);
    vboxEvo->addWidget (evolution_If_Hungry_N);
    groupBoxEvo->setLayout(vboxEvo);
    groupBoxEvo ->setStyleSheet ("QGroupBox { color:white;font-size:25px;background:white rgba(0, 20, 255,30);}");
    grille1->addWidget (groupBoxEvo, x+7, y,6, 1);
 }
 void MenuGui::isThereAQueen(QGridLayout * grille1, int x, int y,int colun){
    std::vector<QRadioButton*> buttons;
    groupBoxQ = new QGroupBox ("Is there a Queen?");
    isThereAQueen_yes = new QRadioButton("Yes");
    isThereAQueen_no = new QRadioButton("No");
    isThereAQueen_no->setChecked(true);
    buttons.push_back(isThereAQueen_no);buttons.push_back(isThereAQueen_yes);
    setStyleSheetForRadioButtons(buttons);
    vboxQ = new QVBoxLayout;
    vboxQ->addWidget (isThereAQueen_yes);
    vboxQ->addWidget (isThereAQueen_no);
    groupBoxQ->setLayout(vboxQ);
    groupBoxQ ->setStyleSheet ("QGroupBox { color:white;font-size:25px;background:white rgba(0, 20, 255,30);}");
    grille1->addWidget (groupBoxQ, x, y, colun,1);
}
    void MenuGui::cocoon_Hatching_Time_Spin(QGridLayout *grille1)
{
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >laps of time before hatching-pupa </h3>");
    grille1 -> addWidget (diseaceP, 30,0);
    cocoon_Hatching_Time = new QSpinBox;
    cocoon_Hatching_Time -> setRange(2, 5000);
    cocoon_Hatching_Time ->setSingleStep(1);
    cocoon_Hatching_Time ->setValue(500);
    grille1->addWidget (cocoon_Hatching_Time, 30,1);
 }
 void MenuGui::normal_Trace_Laps_Spin(QGridLayout *grille1)
 {
    QLabel *diseaceP =  new QLabel("<h3 style='font-family:arial; color:black;' >normal trace duration </h3>");
    grille1 -> addWidget (diseaceP, 18,0);
    normal_Trace_Laps = new QSpinBox;
    normal_Trace_Laps -> setRange(0, 1000);
    normal_Trace_Laps ->setSingleStep(1);
    normal_Trace_Laps ->setValue(200);
    grille1->addWidget (normal_Trace_Laps, 18,1);
 }

////////////////////////////////////////////////
void MenuGui::first_groupe(QGridLayout * grille1, int x, int y)
{
    std::vector<QRadioButton*> buttons;
    groupBox1 = new QGroupBox ("Larva Number");
    larva_number_high = new QRadioButton("High (9)");
    larva_number_high->setChecked(false);
    larva_number_normal = new QRadioButton("Normal (4)");
    larva_number_normal->setChecked(true);
    larva_number_low = new QRadioButton("Low (1)");
    larva_number_low->setChecked(false);
    buttons.push_back(larva_number_normal);buttons.push_back(larva_number_low);buttons.push_back(larva_number_high);
    setStyleSheetForRadioButtons(buttons);
    vbox1 = new QVBoxLayout;
    vbox1->addWidget (larva_number_high);
    vbox1->addWidget (larva_number_normal);
    vbox1->addWidget (larva_number_low);
    groupBox1->setLayout(vbox1);
    groupBox1 ->setStyleSheet ("QGroupBox { color:white;font-size:25px;background:red rgba(0, 20, 255,30);}");
    grille1->addWidget (groupBox1, x,y);
}

void MenuGui::second_groupe(QGridLayout *grille1, int x, int y)
{
    std::vector<QRadioButton*> buttons;
    groupBox2 = new QGroupBox ("Ant Number");
    ant_number_high = new QRadioButton("High (50)");
    ant_number_normal = new QRadioButton("Normal (20)");
    ant_number_normal->setChecked(true);
    ant_number_low = new QRadioButton("Low (5)");
    buttons.push_back(ant_number_normal);buttons.push_back(ant_number_low);buttons.push_back(ant_number_high);
    setStyleSheetForRadioButtons(buttons);
    vbox2 = new QVBoxLayout;
    vbox2->addWidget (ant_number_high);
    vbox2->addWidget (ant_number_normal);
    vbox2->addWidget (ant_number_low);
    groupBox2->setLayout(vbox2);
    groupBox2 ->setStyleSheet ("QGroupBox { color:white;font-size:25px;background:white rgba(0, 20, 255,30);}");
    grille1->addWidget (groupBox2, x, y);
}
void MenuGui::third_groupe (QGridLayout * grille1, int x, int y){
    std::vector<QRadioButton*> buttons;
    groupBox3 = new QGroupBox ("Ant's Intelligence");
    Intelligence_high = new QRadioButton("High (7)");
    Intelligence_normal = new QRadioButton("Normal (5)");
    Intelligence_normal->setChecked(true);
    Intelligence_low = new QRadioButton("Low (3)");
    buttons.push_back(Intelligence_normal);buttons.push_back(Intelligence_low);buttons.push_back(Intelligence_high);
    setStyleSheetForRadioButtons(buttons);
    vbox3 = new QVBoxLayout;
    vbox3->addWidget (Intelligence_high);
    vbox3->addWidget (Intelligence_normal);
    vbox3->addWidget (Intelligence_low);
    groupBox3->setLayout(vbox3);
    groupBox3 ->setStyleSheet ("QGroupBox { color:white;font-size:25px;background:white rgba(0, 20, 255,30);}");
    grille1->addWidget (groupBox3, x, y);

}
void MenuGui::fourth_groupe (QGridLayout * grille1, int x, int y){
    std::vector<QRadioButton*> buttons;
    groupBox14 = new QGroupBox ("Hungry's resistance");
    Hungry_high = new QRadioButton("High");
    Hungry_normal = new QRadioButton("Normal");
    Hungry_normal->setChecked(true);
    Hungry_low = new QRadioButton("Low");
    buttons.push_back(Hungry_normal);buttons.push_back(Hungry_low);buttons.push_back(Hungry_high);
    setStyleSheetForRadioButtons(buttons);
    vbox14 = new QVBoxLayout;
    vbox14->addWidget (Hungry_high);
    vbox14->addWidget (Hungry_normal);
    vbox14->addWidget (Hungry_low);
    groupBox14->setLayout(vbox14);
    groupBox14 ->setStyleSheet ("QGroupBox { color:white;font-size:25px;background:white rgba(0, 20, 255,30);}");
    grille1->addWidget (groupBox14, x, y);

}

void MenuGui::fifth_groupe (QGridLayout * grille1, int x, int y){
    std::vector<QRadioButton*> buttons;
    groupBox5 = new QGroupBox ("Map Choice");
   
    normal_map = new QRadioButton("Normal");
    normal_map->setChecked(true);
    low_map = new QRadioButton("Small");
 	buttons.push_back(normal_map);buttons.push_back(low_map);
    setStyleSheetForRadioButtons(buttons);
    vbox5 = new QVBoxLayout;
   
   
    vbox5->addWidget (normal_map);
    vbox5->addWidget (low_map);
    groupBox5->setLayout(vbox5);
    groupBox5 ->setStyleSheet ("QGroupBox { color:white;font-size:25px;background:white rgba(0, 20, 255,30);}");
    grille1->addWidget (groupBox5, x, y);

}

void MenuGui::eigth_groupe (QGridLayout * grille1, int x, int y){
    std::vector<QRadioButton*> buttons;
    groupBox8 = new QGroupBox ("Queen's lifetime");
    queenLifetime_high = new QRadioButton("High (13000)");
    queenLifetime_normal = new QRadioButton("Normal (9000)");
    queenLifetime_normal->setChecked(true);
    queenLifetime_low = new QRadioButton("Low (4500)");
    buttons.push_back(queenLifetime_normal);buttons.push_back(queenLifetime_low);buttons.push_back(queenLifetime_high);
    setStyleSheetForRadioButtons(buttons);
    vbox8 = new QVBoxLayout;
    vbox8->addWidget (queenLifetime_high);
    vbox8->addWidget (queenLifetime_normal);
    vbox8->addWidget (queenLifetime_low);
    groupBox8->setLayout(vbox8);
    groupBox8->setStyleSheet ("QGroupBox { color:white;font-size:25px;background:white rgba(0, 20, 255,30);}");
    grille1->addWidget (groupBox8, x, y);

}
void MenuGui::nineth_groupe (QGridLayout * grille1, int x, int y){
    std::vector<QRadioButton*> buttons;
    groupBox9 = new QGroupBox ("Stimuli Spreading");
    stimuli_spreading_high = new QRadioButton("High (0.5)");
    stimuli_spreading_normal = new QRadioButton("Normal (1)");
    stimuli_spreading_normal->setChecked(true);
    stimuli_spreading_low = new QRadioButton("Low (1.5)");
    buttons.push_back(stimuli_spreading_normal);buttons.push_back(stimuli_spreading_low);buttons.push_back(stimuli_spreading_high);
    setStyleSheetForRadioButtons(buttons);
    vbox9 = new QVBoxLayout;
    vbox9->addWidget (stimuli_spreading_high);
    vbox9->addWidget (stimuli_spreading_normal);
    vbox9->addWidget (stimuli_spreading_low);
    groupBox9->setLayout(vbox9);
    groupBox9->setStyleSheet ("QGroupBox { color:white;font-size:25px;background:white rgba(0, 20, 255,30);}");
    grille1->addWidget (groupBox9, x, y);

}
void MenuGui::tenth_groupe (QGridLayout * grille1, int x, int y){
    std::vector<QRadioButton*> buttons;
    groupBox10 = new QGroupBox ("Ant's Lifetime");
    ant_lifetime_high = new QRadioButton("High (4500)");
    ant_lifetime_normal = new QRadioButton("Normal (3000)");
    ant_lifetime_normal->setChecked(true);
    ant_lifetime_low = new QRadioButton("Low (1500)");
    buttons.push_back(ant_lifetime_normal);buttons.push_back(ant_lifetime_low);buttons.push_back(ant_lifetime_high);
    setStyleSheetForRadioButtons(buttons);
    vbox10 = new QVBoxLayout;
    vbox10->addWidget (ant_lifetime_high);
    vbox10->addWidget (ant_lifetime_normal);
    vbox10->addWidget (ant_lifetime_low);
    groupBox10->setLayout(vbox10);
    groupBox10 ->setStyleSheet ("QGroupBox { color:white;font-size:25px;background:white rgba(30, 80, 200,70);}");
    grille1->addWidget (groupBox10, x, y);

}
void MenuGui::eleventh_groupe (QGridLayout * grille1, int x, int y){
    std::vector<QRadioButton*> buttons;
    groupBox11 = new QGroupBox ("Larva State Duration");
    larva_state_duration_high = new QRadioButton("High");
    larva_state_duration_normal = new QRadioButton("Normal");
    larva_state_duration_normal->setChecked(true);
    larva_state_duration_low = new QRadioButton("Low");
    buttons.push_back(larva_state_duration_normal);buttons.push_back(larva_state_duration_low);buttons.push_back(larva_state_duration_high);
    setStyleSheetForRadioButtons(buttons);
    vbox11 = new QVBoxLayout;
    vbox11->addWidget (larva_state_duration_high);
    vbox11->addWidget (larva_state_duration_normal);
    vbox11->addWidget (larva_state_duration_low);
    groupBox11->setLayout(vbox11);
    groupBox11->setStyleSheet ("QGroupBox { color:white;font-size:25px;background:white rgba(0, 20, 255,30);}");
    grille1->addWidget (groupBox11, x, y);

}
void MenuGui::twelveth (QGridLayout * grille1, int x, int y){
    std::vector<QRadioButton*> buttons;
    groupBox12 = new QGroupBox ("Eggs State Duration");
    eggs_state_duration_high = new QRadioButton("High");
    eggs_state_duration_normal = new QRadioButton("Normal");
    eggs_state_duration_normal->setChecked(true);
    eggs_state_duration_low = new QRadioButton("Low");
    buttons.push_back(eggs_state_duration_normal);buttons.push_back(eggs_state_duration_low);buttons.push_back(eggs_state_duration_high);
    setStyleSheetForRadioButtons(buttons);
    vbox12 = new QVBoxLayout;
    vbox12->addWidget (eggs_state_duration_high);
    vbox12->addWidget (eggs_state_duration_normal);
    vbox12->addWidget (eggs_state_duration_low);
    groupBox12->setLayout(vbox12);
    groupBox12->setStyleSheet ("QGroupBox { color:white;font-size:25px;background:white rgba(0, 20, 255,30);}");
    grille1->addWidget (groupBox12, x, y);

}
void MenuGui::thirteenth (QGridLayout * grille1, int x, int y){
    std::vector<QRadioButton*> buttons;
    groupBox13 = new QGroupBox ("Queen's Fecondity");
    qweens_fecondity_high = new QRadioButton("High");
    qweens_fecondity_normal = new QRadioButton("Normal");
    qweens_fecondity_normal->setChecked(true);
    qweens_fecondity_low = new QRadioButton("Low");
    buttons.push_back(qweens_fecondity_normal);buttons.push_back(qweens_fecondity_low);buttons.push_back(qweens_fecondity_high);
    setStyleSheetForRadioButtons(buttons);
    vbox13 = new QVBoxLayout;
    vbox13->addWidget (qweens_fecondity_high);
    vbox13->addWidget (qweens_fecondity_normal);
    vbox13->addWidget (qweens_fecondity_low);
    groupBox13->setLayout(vbox13);
    groupBox13 ->setStyleSheet ("QGroupBox { color:white;font-size:25px;background:white rgba(0, 20, 255,30);}");
    grille1->addWidget (groupBox13, x, y);

}

void MenuGui::eggs_number(QGridLayout * grille1, int x, int y){
    std::vector<QRadioButton*> buttons;
    groupBoxEgg = new QGroupBox ("Egg's number");
    eggs_number_high = new QRadioButton("High (20)");
    eggs_number_normal = new QRadioButton("Normal (10)");
    eggs_number_low = new QRadioButton("Low (5)");
    eggs_number_normal->setChecked(true);
    buttons.push_back(eggs_number_normal);buttons.push_back(eggs_number_high);buttons.push_back(eggs_number_low);
    setStyleSheetForRadioButtons(buttons);
    vboxEgg = new QVBoxLayout;
    vboxEgg->addWidget(eggs_number_high);
    vboxEgg->addWidget(eggs_number_normal);
    vboxEgg->addWidget(eggs_number_low);
    groupBoxEgg->setLayout(vboxEgg);
    groupBoxEgg ->setStyleSheet("QGroupBox { color:white;font-size:25px;background:white rgba(0, 20, 255,30);}");
    grille1->addWidget(groupBoxEgg, x, y);
}
void MenuGui::get_user_input()
{   if(_MenuType==1)
    {

        if (ant_number_high->isChecked())
        {
            antNumber = 50;//ok
            
        }
        if (ant_number_normal->isChecked())
        {
            antNumber =20;
            
        }
        if (ant_number_low->isChecked())
        {
            antNumber = 5;
            
        }
        if (Intelligence_high->isChecked())
        {
            antIntel= 7;//ok
        }
        if (Intelligence_normal->isChecked())
        {
            antIntel= 5;
        }
        if (Intelligence_low->isChecked())
        {
            antIntel= 3;
        }
       
        if (normal_map->isChecked())
        {
            _map = "map4.txt";
        }
        if (low_map->isChecked())
        {
            _map = "map5.txt";
        }
        if (isThereAQueen_no->isChecked()){
            _isThereAQueen = false;//ok
        }
        if (isThereAQueen_yes->isChecked()){
            _isThereAQueen = true;
        }
        if (eggs_number_high->isChecked()){
            _eggsNumber = 20;//ok
        }
        if (eggs_number_normal->isChecked()){
            _eggsNumber = 10;
        }
        if (eggs_number_low->isChecked()){
            _eggsNumber = 5;
        }
        if (queenLifetime_high->isChecked()){
            _queenLifetime = 13000;//ok
        }
        if (queenLifetime_normal->isChecked()){
            _queenLifetime = 9000;
        }
        if (queenLifetime_low->isChecked()){
            _queenLifetime = 4500;
        }
        if (stimuli_spreading_high->isChecked())
        {   
            stimuliSpreading= 0.5;//ok
        }
        if (stimuli_spreading_normal->isChecked())
        {
            stimuliSpreading= 1;
        }
        if (stimuli_spreading_low->isChecked())
        {
            stimuliSpreading= 1.5;
        }
        if (ant_lifetime_high->isChecked())
        {
            liveTime= 4500;//ok
        }
        if (ant_lifetime_normal->isChecked())
        {
            liveTime= 3000;
        }
        if (ant_lifetime_low->isChecked())
        {
            liveTime= 1500;
        }   

        if (larva_state_duration_high->isChecked())
        {
            larvaTime= 300;//ok
        }
        if(larva_state_duration_normal->isChecked())
        {   
            larvaTime= 225;
        }
        if (larva_state_duration_low->isChecked())
        {
            larvaTime= 150;
        }
        if (eggs_state_duration_high->isChecked())
        {
            eggTime= 1500;//ok
        }
        if (eggs_state_duration_normal->isChecked())
        {
            eggTime= 1125;
        }
        if (eggs_state_duration_low->isChecked())
        {
            eggTime= 750;
        }
        if (qweens_fecondity_high->isChecked())
        {
            turnLayQueen= 250 ;//ok
        }
        if (qweens_fecondity_normal->isChecked())
        {
            turnLayQueen=500 ;
        }
        if(qweens_fecondity_low->isChecked())
        {
            turnLayQueen= 1000 ;
        }
        if(larva_number_high->isChecked()){
            _larvasNumber = 9;
        }
        if(larva_number_normal->isChecked()){
            _larvasNumber = 4;
        }
        if(larva_number_low->isChecked()){
            _larvasNumber = 1;
        }
        stimuliSpreading2=0.1f;
        _hungryResistance=2;
        _queenhungryResistance=2;
        seedNutrition=2;
        leafNutrition=3;
        antFrequenceMovement=1;
        _queenHungry=800;
        _antFrequenceHungry=300;
        queenFrequenceMovement=1;
        pupaNumber=0;
        pupaTime=500;
        evolutionIfHungry=true;
        larvaFrequenceHungry=100;
        _hungryResistanceLarva=2;
        foodTraceLifetime= 150;
        ignoringTrace= 20;
        normalTraceLaps= 200;
        launch_simulation();
    
    }
    else {
    	
        //ant
        antNumber= ant_number->value();//ok
        antIntel= antIntelligence->value();//ok
        antFrequenceMovement =ant_Frequence_Movement->value();//ok
        _antFrequenceHungry=ant_Frequence_Hungry->value();//ok
        _hungryResistance= hungry_Resistance->value();//ok
        liveTime=ant_Lifetime->value();//ok
        foodTraceLifetime= food_Trace_Lifetime->value();//ok
        ignoringTrace= ignoring_Trace->value();//ok
        normalTraceLaps= normal_Trace_Laps->value();//ok
		

        //queen
        if (isThereAQueen_no->isChecked()){
            _isThereAQueen = false;//ok
        }
        if (isThereAQueen_yes->isChecked()){
            _isThereAQueen = true;
        }
        queenIntel=queen_Intelligence->value();//ok
        queenFrequenceMovement=queen_Frequence_movement->value();//ok
        turnLayQueen= lay_Frequence->value();//ok
        _queenhungryResistance=hungry_Resistance_Queen->value();//ok
        _queenHungry=queen_Frequence_Hungry->value();//ok



        //stimuli
        stimuliSpreading= StimuliSpreading_O->value();//ok
        stimuliSpreading2= StimuliSpreading_I->value();//ok

        //food
        seedNutrition=seedQuantity->value();//ok
        leafNutrition=leafQuantity->value();//ok


        //egg
        _eggsNumber=eggs_Number->value();//ok
        eggTime=before_Hatching->value();//ok

        //larve
        _larvasNumber=larva_number->value();//ok
        larvaTime=before_LarvaEvo->value();//ok
        larvaFrequenceHungry=larva_Frequence_Hungry->value();//ok
        _hungryResistanceLarva=larva_Hungry_Resistance->value();//ok
        evolutionIfHungry = false;
        if (evolution_If_Hungry_Y->isChecked()){
            evolutionIfHungry = true;
        }

        //pupa
        pupaNumber=number_Of_Puppa->value();//ok
        pupaTime=cocoon_Hatching_Time->value();//ok
        _map = "map4.txt";	
        launch_simulation();
        }
}
void MenuGui::fillList()
{

    antsChoiceButton [0]=antSchoice1;antsChoiceButton [1]=antSchoice2;antsChoiceButton[2]=antSchoice3;antsChoiceButton[3]=antSchoice4;
    seedsChoiceButton[0]=seedSchoice1;seedsChoiceButton[1]=seedSchoice2;seedsChoiceButton[2]=seedSchoice3;seedsChoiceButton[3]=seedSchoice4;
    leafsChoiceButton[0]=leafSchoice1;leafsChoiceButton[1]=leafSchoice2;leafsChoiceButton[2]=leafSchoice3;leafsChoiceButton[3]=leafSchoice4;
    grassChoiceButton[0]=grassSchoice1;grassChoiceButton[1]=grassSchoice2;grassChoiceButton[2]=grassSchoice3;


    antsChoiceimages[0]="ant1.png";antsChoiceimages[1]="cat1Ant.png";antsChoiceimages[2]="cat2Ant.png";antsChoiceimages[3]="cat3Ant.png";
    seedsChoiceimages[0]="seed.png";seedsChoiceimages[1]="seed2.png";seedsChoiceimages[2]="seed3.png";seedsChoiceimages[3]="seed4.png";
    leafsChoiceimages[0]="leaf.png";leafsChoiceimages[1]="leaf2.png";leafsChoiceimages[2]="leaf3.png";leafsChoiceimages[3]="leaf4.png";
}

int MenuGui::launch_simulation()
{
    std::cout<<"Debut Simulation"<<std::endl;
    fillList();
    if (fenetre2){
    	a->astop();
    	delete _world;
    	delete fenetre2;
    	delete a;

    }
    fenetre2 = new QWidget();
    
    fenetre2->setFixedSize(rect.height()-100,rect.height()-100);
    fenetre2->setWindowTitle("MAP");
   
    for (unsigned int i=0; i<3; i++){

        if(grassChoiceButton [i]->isChecked() and i==0){
            fenetre2->setStyleSheet("background-image: url(grass.jpg)");
        }
        else if(grassChoiceButton [i]->isChecked() and i==1){
            fenetre2->setStyleSheet("background-image: url(grass1.jpg)");
        }
        else if(grassChoiceButton [i]->isChecked() and i==2){
            fenetre2->setStyleSheet("background-image: url(grass2.jpg)");
        }
    }

    QGridLayout *lol = new QGridLayout(fenetre2);
    lol->setSpacing(0);
    lol->setMargin(0);

    MapGUI *balbla = new MapGUI(_map,lol, getMapObjectImage(leafsChoiceButton, leafsChoiceimages), getMapObjectImage(seedsChoiceButton, seedsChoiceimages),mainWindow,stimuliSpreading,stimuliSpreading2,seedNutrition,leafNutrition);
    _world = new WorldGUI(balbla,getMapObjectImage(antsChoiceButton, antsChoiceimages),"Egg.png","Larva.png","Pupa.png",antIntel,liveTime,eggTime,larvaTime,pupaTime,antFrequenceMovement,_antFrequenceHungry,evolutionIfHungry,larvaFrequenceHungry,_hungryResistanceLarva,foodTraceLifetime,ignoringTrace,normalTraceLaps);
    if(_isThereAQueen){
        _world->createQueen(queenIntel,_queenHungry,_queenhungryResistance,queenFrequenceMovement,_queenLifetime,turnLayQueen,balbla->getSpawnPos().at(3),"queenant.png");
    }
    for(int i=0;i<antNumber;i++){
        _world->createAnt(antIntel,_antFrequenceHungry,_hungryResistance,antFrequenceMovement,liveTime,balbla->getSpawnPos().at(i%3),foodTraceLifetime,ignoringTrace,normalTraceLaps);
    }
    for(int i=0;i<_eggsNumber;i++){
        _world->createEgg(eggTime,balbla->getSpawnPos().at((i%3)+3));
    }
    for(int i=0;i<_larvasNumber;i++){
        _world->createLarva(larvaFrequenceHungry,_hungryResistanceLarva,larvaTime,balbla->getSpawnPos().at((i%3)+3),evolutionIfHungry);
    }
    for(int i=0;i<pupaNumber;i++){
        _world->createPupa(pupaTime,balbla->getSpawnPos().at((i%3)+3));
    }
    std::cout<<"fin Simulation"<<std::endl;
    
    a=new MyThread(_world);
    a->start();

    fenetre2->show();

    return 0;
}


void foo(World* world){
    world->management();
}

std::string MenuGui::getMapObjectImage(QRadioButton *buttonList[], std::string imageList[]){
    for (unsigned int i=0; i<4; i++){

        if(buttonList[i]->isChecked() and i==0){
            return imageList[i];
        }
        else if(buttonList[i]->isChecked() and i==1){
            return imageList[i];
        }
        else if(buttonList[i]->isChecked() and i==2){
            return imageList[i];
        }
        else if(buttonList[i]->isChecked() and i==3){
            return imageList[i];
        }
    }
    return "";
}
