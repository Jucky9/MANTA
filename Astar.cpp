#include "Astar.hpp"

Astar::Astar(Square* d,Square* f,int weight):_working(),_f(f),previousSquare(){
	if(d!=f){
		findPath(d,f,weight);
	}

}
void Astar::findPath(Square*d,Square*f,int weight){
	Node current(d,0);
	int newCost;
	_working.push(current);//point de départ
	Square* next(nullptr);
	
	std::map<Square*, int> coutSquare;
	std::map<Square*, int> blockSquare;
	previousSquare[d]=nullptr;
	coutSquare[d]=0;


	while(_working.size()){
		current=_working.top();
		_working.pop();
		if(current.getSquare()==f)
			break;

		for(int i=0;i<8;i++){
			next=current.getSquare()->getNeighbor(i);
			if(next and next->canGoIn(weight) and blockSquare.find(next)==blockSquare.end()){
				newCost=coutSquare[current.getSquare()]+heuristic(current.getSquare(),next);
				if(coutSquare.find(next)==coutSquare.end() or newCost<coutSquare[next]){
					_working.push(Node(next,heuristic(next,f)));
					previousSquare[next]=current.getSquare();
					coutSquare[next]=newCost+heuristic(next,f);
				}
				blockSquare[next]=0;
			}
		}
	}
}
std::vector<Square*> Astar::getPath(){
	std::vector<Square*> temp;
	if(!previousSquare.size())//chemin vide on est au point
		return temp;
	temp.push_back(_f);
	std::vector<Square*>::iterator it(temp.begin());
	Square* current(_f);
	while (previousSquare[current]){
		current=previousSquare[current];
		it=temp.insert(it,current);
	}
	temp.erase(temp.begin());//supresion de notre position
	return temp;
}


Astar::Node::Node(Square* pos,int cout):_pos(pos),_cout(cout){}
int heuristic(Square* a,Square* b){
	return abs(a->getPosI()-b->getPosI())+abs(a->getPosJ()-b->getPosJ());
}
bool Astar::Node::isSmal(Astar::Node const& b)const{
	return _cout<b._cout;
}
bool Astar::Node::isEgal(Astar::Node const& b)const{
	return _cout==b._cout;
}
bool operator<(Astar::Node const& a, Astar::Node const& b){
	return a.isSmal(b);
}
bool operator>(Astar::Node const& a, Astar::Node const& b){
	return !(a<b);
}
bool operator==(Astar::Node const& a, Astar::Node const& b){
	return a.isEgal(b);
}
bool operator!=(Astar::Node const& a, Astar::Node const& b){
	return !(a==b);
}
int Astar::Node::getCout(){
	return _cout;
}
Square* Astar::Node::getSquare(){
	return _pos;
}