#ifndef LeafGUI_h
#define LeafGUI_h

#include "ObjectGUI.hpp"
#include <QPixmap>
#include "Leef.hpp"


class LeafGUI: public Leef, public ObjectGUI{
public:
	LeafGUI(int,bool,int,std::string);
	virtual ~LeafGUI()=default;
};



#endif
