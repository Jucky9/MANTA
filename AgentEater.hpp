#ifndef AGENTEATER_HPP
#define AGENTEATER_HPP

#include "Agent.hpp"
#include "Food.hpp"


class AgentEater: public Agent{
	public:
		AgentEater(int,int,int,int,int);
		AgentEater(int,int,int,int,Square*,int);
		virtual ~AgentEater()=default;
		virtual bool isHungry();
		virtual int nbrDeathEating();
		virtual int notEating();
		virtual void eat(Food*);
	protected:
		virtual int getTurnHungry();
		virtual void hungry();
		virtual void incTurn()override;
		int _turnEating;
		int _turnHungry;
		bool _isHungry;
		int _notEating;
		int _nbrDeathEating;
		int _typeStimuliFood;
		float _powerStimuliFood;
		int _previousTypeStimuli;
		float _previousPowerStimuli;
};
#endif /* AGENTEATER_HPP */