#ifndef QUEENGUI_HPP
#define QUEENGUI_HPP

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QPixmap>
#include <QPropertyAnimation>
#include <QTransform>
#include <iostream>
#include "ObjectGUI.hpp"
#include "Queen.hpp"
#include "SquareGUI.hpp"
#include <unistd.h>
#define DURATION 100
#define SIZEQUEEN 25


class QueenGUI: public ObjectGUI,public Queen {
	Q_OBJECT
	public:
		QueenGUI(QGridLayout*,int,int,int,int,int,int,Square*,std::string,int=8,int=2);
		virtual ~QueenGUI()=default;
		virtual bool isReady()override;
	private:
		QPropertyAnimation *anim;
		QGridLayout *grid;
		bool _annimeDone;
	protected:
		virtual bool moveSquare(Square*)override;
		virtual void death()override;
	public slots:
		void moveSlot(Square*);
		void endAnimation();
		void deathSlot();
	signals:
		void moveSignal(Square*);
		void deathSignal();
};

#endif