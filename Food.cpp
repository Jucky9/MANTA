#include "Food.hpp"


Food::Food(int weight,bool isMovable,int nbrPortion) : Object(weight,isMovable,1,4), _portion(nbrPortion),_powerNormal(_stimuliPower){}


int Food::getPortion()const{
	return _portion;
}
void Food::beEaten(){
	_portion--;
}
void Food::setPos(Square* pos){
	Object::setPos(pos);
	float maxStimuliPower=_myPosition->getStimuliPower(_stimuliType);
	if(maxStimuliPower>_stimuliPower){
		_stimuliPower=maxStimuliPower+0.1;
	}
	normalStimuli();
}
void Food::toBeCarried(){
	_myPosition->removeObjectOnSquare(this);
	Object::toBeCarried();
	_stimuliPower=_powerNormal;

}
void Food::putOn(Square* pos){
	Object::putOn(pos);
	_myPosition->addObjectOnSquare(this);
}
void Food::incStimuliType(){
	if(_stimuliType==1){
		_stimuliType+=5;
		_stimuliPower=5;
		_powerNormal=_stimuliPower;
	}
	
}
