#include "LarvaGUI.hpp"

LarvaGUI::LarvaGUI(QGridLayout* fenetre,int turnHungry,int nbrDeathEating,int evolveTurn,Square* pos, std::string larvaImage,bool evolutionIfHungry): ObjectGUI(QString(larvaImage.c_str())),Larva(turnHungry,nbrDeathEating,evolveTurn,pos,evolutionIfHungry){
    if(!_label){
        _label = new QLabel();
        _label->setFixedSize(SIZE,SIZE);
        _label->setScaledContents(true);
        _label->setStyleSheet("background:transparent;");
        _label->setPixmap(_pic);
        if(_myPosition){
            fenetre->addWidget(_label,_myPosition->getPosI(),_myPosition->getPosJ());
        }
    }
}

void LarvaGUI::death(){
    _mtx.lock();
    Larva::death();
    emit deathSignal();

}

void LarvaGUI::deathSlot(){
    _namePix="skull.png";
    _pic=QPixmap(_namePix);
    _label->setPixmap(_pic);
    _mtx.unlock();
}
