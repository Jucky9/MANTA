#ifndef Leef_h
#define Leef_h

#include "Object.hpp"
#include "Food.hpp"

class Leef : public Food{
public:
	Leef(int,bool,int);
	void display()override;
	virtual ~Leef()=default;
};
#endif