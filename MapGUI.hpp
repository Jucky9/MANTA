#ifndef MAPGUI_HPP
#define MAPGUI_HPP
#include "SquareGUI.hpp"
#include "LeafGUI.hpp"
#include "SeedGUI.hpp"
#include <QApplication>
#include <QWidget>
#include <QTabWidget>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include <QRadioButton>
#include <QGroupBox>
#include <QGridLayout>
#include <QMainWindow>
#include <QLayout>
#include <iostream>
#include "Map.hpp"
#include "WallGUI.hpp"
#include "GroundGui.hpp"
#include "ClickableLabel.hpp"
#include <vector>


class MapGUI :public QObject, public Map{

	Q_OBJECT
public:
	MapGUI(std::string,QGridLayout*, std::string, std::string, QMainWindow*,float,float,int,int);
	QGridLayout* getGrid();
	~MapGUI();
public slots:
	void showOption(SquareGUI*);
	void seedSchoice (QGridLayout *,int,int);
	void leafSchoice (QGridLayout *,int,int);
	void validateChoices();
	void squareClicked(SquareGUI*);
protected:
	virtual void createObjectsByLine(std::string,int) override;
	QGridLayout* _window;
private:
	std::string leafImage;
	std::string seedImage;	
	QWidget* fenetre = nullptr;
	QMainWindow* mainWindow;
	QRadioButton *seedSchoice1;
	QRadioButton *seedSchoice2;
	QRadioButton *seedSchoice3;
	QRadioButton *seedSchoice4;
	QRadioButton *leafSchoice1;
	QRadioButton *leafSchoice2;
	QRadioButton *leafSchoice3;
	QRadioButton *leafSchoice4;
	QPushButton *validate;
	QPushButton *validate2;
	SquareGUI *selectedSquare = nullptr;
	int i;
	int j;
signals:
	void squareClickedSignal(SquareGUI*);
	void createLeafSignal(int,bool,int,std::string,Square*);
 	void createSeedSignal(int,bool,int,std::string,Square*);
 protected:
 	int _seedNutrition;
    int _leafNutrition;
};
#endif