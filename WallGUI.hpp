#ifndef WallGUI_h
#define WallGUI_h

#include "Wall.hpp"
#include <QApplication>
#include <QWidget>
#include <QTabWidget>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include <QRadioButton>
#include <QGroupBox>
#include <QGridLayout>
#include <QLayout>
#include <iostream>
#include "ClickableLabel.hpp"
class WallGui : public Wall{
	
public:
	WallGui(bool);
	ClickableLabel* returnImage(int,int);
	~WallGui()=default;

private:
	std::string _map_img;
	ClickableLabel* _label;
};
#endif