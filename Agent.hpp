#ifndef AGENT_HPP
#define AGENT_HPP


#include "Stimuli.hpp"
#include "Object.hpp"
#include "Square.hpp"

class Square;

class Agent:public Object{
	public:
		Agent(int,int,int,int=4,float=4.5);
		Agent(int,int,Square*,int,int=4,float=4.5);
		Agent(const Agent&)=delete;
		Agent& operator=(const Agent&)=delete;
		virtual bool isAlive();
		virtual void kill();
		virtual void act()=0;
		virtual void toBeCarried()override;
		virtual void putOn(Square*)override;
		virtual bool isReady();
		//Destructor
		virtual ~Agent()=default;
	protected:
		virtual void death();
		virtual void incTurn();
		virtual void setPos(Square*)override;
		int _turn;
		int _range;
		bool _isAlive;
	private:
		int _deathStimuli;
		float _deathStimuliPower;


};
#endif /* AGENT_HPP */