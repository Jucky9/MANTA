#ifndef SQUAREGUI_HPP
#define SQUAREGUI_HPP

#include "Object.hpp"
#include "Square.hpp"
#include "ObjectGUI.hpp"
#include "WallGUI.hpp"
#include "GroundGui.hpp"
#include "SeedGUI.hpp"
#include "LeafGUI.hpp"
#include "ActiveAgent.hpp"
#include <QPixmap>
#include <typeinfo>
#include <iostream>
#include <vector>


class SquareGUI: public ObjectGUI , public Square {
	Q_OBJECT
public:
	SquareGUI(Object*,int,int,bool,int,float,QGridLayout* =nullptr);
	SquareGUI(int,int,bool,int,float,QGridLayout* =nullptr);
	virtual ~SquareGUI()=default;
	virtual bool addObjectOnSquare(Object *)override;
	virtual void removeObjectOnSquare(Object*)override;
public slots:
	void beClicked();
	void addGUIObjectSlot(Object*);
	void removeGUIObjectSlot(Object*);
signals:
	void clicked(SquareGUI*);
	void addGUIObject(Object*);
	void removeGUIObject(Object*);
private:
	QGridLayout* _window;
};



#endif
