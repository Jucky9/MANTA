#include "AgentEater.hpp"

AgentEater::AgentEater(int weight,int range,int turnHungry, int nbrDeathEating ,int stimuliType): Agent(weight,range,stimuliType),_turnEating(0), _turnHungry(turnHungry+(rand()%(turnHungry/3))),_isHungry(false), _notEating(0),_nbrDeathEating(nbrDeathEating)
						,_typeStimuliFood(3),_powerStimuliFood(8), _previousTypeStimuli(0), _previousPowerStimuli(0){}
	
AgentEater::AgentEater(int weight,int range,int turnHungry, int nbrDeathEating ,Square* pos,int stimuliType): Agent(weight,range,pos,stimuliType),_turnEating(0), _turnHungry(turnHungry+(rand()%(turnHungry/3))),_isHungry(false), _notEating(0),_nbrDeathEating(nbrDeathEating)
						,_typeStimuliFood(3),_powerStimuliFood(8), _previousTypeStimuli(0), _previousPowerStimuli(0){}


void AgentEater::eat(Food* myFood){
	myFood->beEaten();
	_isHungry=false;
	_notEating=0;
	_turnEating=0;
	_stimuliType=_previousTypeStimuli;
	_stimuliPower=_previousPowerStimuli;
	normalStimuli();
}

bool AgentEater::isHungry(){
	return _isHungry;
}

int AgentEater::nbrDeathEating(){
	return _nbrDeathEating;
}

int AgentEater::notEating(){
	return _notEating;
}

void AgentEater::hungry(){
	if(isHungry())
		_notEating++;
	else{
		_isHungry=true;
		_previousTypeStimuli=_stimuliType;
		_previousPowerStimuli=_stimuliPower;
		_stimuliType=_typeStimuliFood;
		_stimuliPower= _powerStimuliFood;
		normalStimuli();
	}
	if(_notEating==_nbrDeathEating)
		death();

}
int AgentEater::getTurnHungry(){
	return _turnHungry;
}
void AgentEater::incTurn(){
	if(_myPosition)
		_turnEating++;
	Agent::incTurn();
}