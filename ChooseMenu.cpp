#include "ChooseMenu.hpp"
chooseMenu::chooseMenu () :fenetre(NULL)
{
    mainWindow = new QMainWindow();
    screen = new QDesktopWidget();
    rect = screen->screenGeometry();
    mainWindow->setGeometry(rect);
    mainWindow->show();
    
}
chooseMenu::~chooseMenu()
{
	if(fenetre)
	{
		delete fenetre;
		fenetre = 0;
	}

}

void chooseMenu::launch_choosing()
{
	fenetre = new QWidget(mainWindow);

	fenetre->setFixedSize(rect.size());
	fenetre->setStyleSheet("background: url(1.jpg)");
	QGridLayout *grille1 = new QGridLayout(fenetre);
	QLabel *titre =  new QLabel("<h1 style='font-family:arial; color:white;'> Welcome To The Manta Project </h1>"
		"<h3 style='font-family:arial; color:white;' >The Project That Simulate Ant's behaviour ...</h3>"
		"<h3 style='font-family:arial; color:white;' >Please Choose Your Mode ...</h3><br><br>");
	grille1 -> addWidget (titre, 0,0);


	QPushButton *validate = new QPushButton(" > Non Scientific Community ");
    //validate->setStyleSheet("background-color: #4CAF50; color: white; font-size:20px; border: #4CAF50; padding: 4px; ");
    QPixmap pixmap("non_scientist.png");
    QIcon ButtonIcon(pixmap);
    validate->setIcon(ButtonIcon);
    validate->setIconSize(pixmap.rect().size());
    validate->setCursor(Qt::PointingHandCursor);
    grille1->addWidget(validate, 2, 0);
    
    QPushButton *validate2 = new QPushButton(" > Scientific Community ");
    //validate2->setStyleSheet("background-color: #4CAF50; color: white; font-size:20px; border: #4CAF50; padding: 4px; ");
    QPixmap pixmap2("scientist.png");
    QIcon ButtonIcon2(pixmap2);
    validate2->setIcon(ButtonIcon2);
    validate2->setIconSize(pixmap2.rect().size());
    validate2->setCursor(Qt::PointingHandCursor);
    grille1->addWidget(validate2, 2, 1);

    grille1->setAlignment(Qt::AlignTop);

    QObject::connect(validate, SIGNAL(clicked()), this, SLOT(nonScientific()));
    QObject::connect(validate2, SIGNAL(clicked()), this, SLOT(Scientific()));
	fenetre -> show(); 

}
void chooseMenu::nonScientific()
{
	//fenetre->close();
	//delete fenetre;
	MenuGui *menuObj = new MenuGui(1);
	menuObj -> launch_menu();
    
}
void chooseMenu::Scientific()
{

   	//fenetre->close();
	//delete fenetre;
	MenuGui *menuObj2 = new MenuGui(2);
	menuObj2 -> launch_menu();
}
